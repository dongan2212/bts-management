package com.web.model;

/**
 * Created by Administrator on 10/24/2017.
 */
public class PhuongXaForm {
    private Long id;
    private String name;
    private Long idQuanHuyen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdQuanHuyen() {
        return idQuanHuyen;
    }

    public void setIdQuanHuyen(Long idQuanHuyen) {
        this.idQuanHuyen = idQuanHuyen;
    }
}
