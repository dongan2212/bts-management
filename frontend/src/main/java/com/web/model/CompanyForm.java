package com.web.model;

/**
 * Created by Administrator on 10/20/2017.
 */
public class CompanyForm {
    private Long id;
    private String name;
    private String markericon;
    private String phone;
    private String email;
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarkericon() {
        return markericon;
    }

    public void setMarkericon(String markericon) {
        this.markericon = markericon;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
