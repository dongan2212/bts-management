package com.web.model;

/**
 * Created by Administrator on 10/22/2017.
 */
public class QuanHuyenForm {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
