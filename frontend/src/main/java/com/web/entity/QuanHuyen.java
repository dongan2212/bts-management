package com.web.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

/**
 * Created by Bi on 6/10/2017.
 */
@Entity
@Table(name = "quanhuyen")
public class QuanHuyen {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "datecreated")
    private Date dateCreated;
    @Column(name = "dateupdated")
    private Date dateUpdated;
    @OneToMany(mappedBy = "idQuanHuyen")
    private Set<PhuongXa> PhuongXaByIdQuanHuyen;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Set<PhuongXa> getPhuongXaByIdQuanHuyen() {
        return PhuongXaByIdQuanHuyen;
    }

    public void setPhuongXaByIdQuanHuyen(Set<PhuongXa> phuongXaByIdQuanHuyen) {
        PhuongXaByIdQuanHuyen = phuongXaByIdQuanHuyen;
    }
}
