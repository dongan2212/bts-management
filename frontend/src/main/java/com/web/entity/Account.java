package com.web.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Bi on 5/10/2017.
 */
@Entity
@Table(name = "account")
public class Account implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "birthday")
    private String birthday;
    @ManyToOne
    @JoinColumn(name = "idrole")
    private Role idRole;
    @OneToMany(mappedBy = "userCreated")
    private Set<BTS> BTSbyUserCreate;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Role getIdRole() {
        return idRole;
    }

    public void setIdRole(Role idRole) {
        this.idRole = idRole;
    }

    public Set<BTS> getBTSbyUserCreate() {
        return BTSbyUserCreate;
    }

    public void setBTSbyUserCreate(Set<BTS> BTSbyUserCreate) {
        this.BTSbyUserCreate = BTSbyUserCreate;
    }
}
