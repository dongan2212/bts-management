package com.web.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Bi on 6/10/2017.
 */
@Entity
@Table(name = "bts")
public class BTS {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "longtitude")
    private double longtitude;
    @Column(name = "latitude")
    private double latitude;
    @Column(name = "height")
    private double height;
    @Column(name = "status")
    private  int status;
    @Column(name = "infomation")
    private  String information;
    @Column(name = "note")
    private  String note;
    @Column(name = "datecreated")
    private Date dateCreated;
    @Column(name = "dateupdated")
    private Date dateUpdated;
    @ManyToOne
    @JoinColumn(name = "usercreated")
    private Account userCreated;
    @ManyToOne
    @JoinColumn(name = "idowner")
    private Company idOwner;
    @ManyToOne
    @JoinColumn(name = "iduser")
    private Company idUser;
    @ManyToOne
    @JoinColumn(name = "idphuong")
    private PhuongXa idPhuongXa;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Account getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(Account userCreated) {
        this.userCreated = userCreated;
    }

    public Company getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(Company idOwner) {
        this.idOwner = idOwner;
    }

    public Company getIdUser() {
        return idUser;
    }

    public void setIdUser(Company idUser) {
        this.idUser = idUser;
    }

    public PhuongXa getIdPhuongXa() {
        return idPhuongXa;
    }

    public void setIdPhuongXa(PhuongXa idPhuongXa) {
        this.idPhuongXa = idPhuongXa;
    }
}
