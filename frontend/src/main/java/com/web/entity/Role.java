package com.web.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Bi on 5/10/2017.
 */
@Entity
@Table(name = "roles")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "rolename")
    private String roleName;
    @Column(name = "detail")
    private String detail;
    @OneToMany(mappedBy = "idRole")
    private Set<Account> accountByIdRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
       this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Set<Account> getAccountByIdRole() {
        return accountByIdRole;
    }

    public void setAccountByIdRole(Set<Account> accountByIdRole) {
        this.accountByIdRole = accountByIdRole;
    }
}