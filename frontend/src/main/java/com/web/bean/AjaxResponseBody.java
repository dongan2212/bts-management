package com.web.bean;

import com.web.dto.BtsDTO;

import java.util.List;

/**
 * Created by duyle on 3/16/17.
 */
public class AjaxResponseBody {

    private List<BtsDTO> btsDTOs;

    public List<BtsDTO> getBtsDTOList() {
        return btsDTOs;
    }

    public void setBtsDTOList(List<BtsDTO> btsDTOs) {
        this.btsDTOs = btsDTOs;
    }
}
