package com.web.dto;

import java.sql.Date;

/**
 * Created by Bi on 7/10/2017.
 */
public class BtsDTO {

    private Long id;
    private String name;
    private String address;
    private double longtitude;
    private double latitude;
    private double height;
    private int status;
    private String information;
    private String note;
    private String userCreated;
    private Long idOwner;
    private Long idUser;
    private Long idPhuongXa;

    /*Không Truyền */
    private String nameCreator;
    private String nameOwner;
    private String nameUser;
    private String namePhuongXa;
    private Long idQuanHuyen;
    private String nameQuanHuyen;
    private Date dateCreated;
    private Date dateUpdated;
    private String markerIconBTS;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getNameCreator() {
        return nameCreator;
    }

    public void setNameCreator(String nameCreator) {
        this.nameCreator = nameCreator;
    }

    public Long getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(Long idOwner) {
        this.idOwner = idOwner;
    }

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getMarkerIconBTS() {
        return markerIconBTS;
    }

    public void setMarkerIconBTS(String markerIconBTS) {
        this.markerIconBTS = markerIconBTS;
    }

    public Long getIdPhuongXa() {
        return idPhuongXa;
    }

    public void setIdPhuongXa(Long idPhuongXa) {
        this.idPhuongXa = idPhuongXa;
    }

    public String getNamePhuongXa() {
        return namePhuongXa;
    }

    public void setNamePhuongXa(String namePhuongXa) {
        this.namePhuongXa = namePhuongXa;
    }

    public Long getIdQuanHuyen() {
        return idQuanHuyen;
    }

    public void setIdQuanHuyen(Long idQuanHuyen) {
        this.idQuanHuyen = idQuanHuyen;
    }

    public String getNameQuanHuyen() {
        return nameQuanHuyen;
    }

    public void setNameQuanHuyen(String nameQuanHuyen) {
        this.nameQuanHuyen = nameQuanHuyen;
    }
}


