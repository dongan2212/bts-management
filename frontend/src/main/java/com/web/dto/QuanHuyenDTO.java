package com.web.dto;

import java.sql.Date;

/**
 * Created by Bi on 6/10/2017.
 */
public class QuanHuyenDTO {
    /*Khi create hoặc update, các tham số cần truyền vào từ backend là (id), name*/
    private Long id;
    private String name;
    private Date dateCreated;
    private Date dateUpdated;
    private int quantityBTS;
    private int quantityPhuongXa;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getQuantityBTS() {
        return quantityBTS;
    }

    public void setQuantityBTS(int quantityBTS) {
        this.quantityBTS = quantityBTS;
    }

    public int getQuantityPhuongXa() {
        return quantityPhuongXa;
    }

    public void setQuantityPhuongXa(int quantityPhuongXa) {
        this.quantityPhuongXa = quantityPhuongXa;
    }
}
