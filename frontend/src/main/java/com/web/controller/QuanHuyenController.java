package com.web.controller;

import com.web.bean.AuthTokenInfo;
import com.web.dto.QuanHuyenDTO;
import com.web.model.QuanHuyenForm;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/14/2017.
 */
@PropertySource("classpath:url.properties")
@Controller
public class QuanHuyenController {

    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    private AuthTokenInfo tokenInfo;

    @RequestMapping(value = "/quanhuyens", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView getAllQuanHuyens(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen");
            builder.setParameter("access_token",tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
//                JSONObject object = parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    ModelAndView view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<QuanHuyenDTO> quanhuyens = new ArrayList<>();
                    QuanHuyenDTO quanhuyenDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        quanhuyenDTO = new QuanHuyenDTO();
                        quanhuyenDTO.setId((Long) jsonObject.get("id"));
                        quanhuyenDTO.setName((String) jsonObject.get("name"));
                        quanhuyenDTO.setQuantityPhuongXa(Integer.parseInt(jsonObject.get("quantityPhuongXa").toString()));
                        quanhuyenDTO.setQuantityBTS(Integer.parseInt(jsonObject.get("quantityBTS").toString()));
                        quanhuyens.add(quanhuyenDTO);
                    }
                    ModelAndView view = new ModelAndView("manageQuanHuyens");
                    view.addObject("quanhuyens", quanhuyens);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/quanhuyens/addNewQuanHuyen", method = RequestMethod.GET)
    public ModelAndView addNewQuanHuyen(ModelAndView model) {
        QuanHuyenForm quanhuyenForm = new QuanHuyenForm();
        model.addObject("quanhuyenForm", quanhuyenForm);
        model.setViewName("quanhuyenForm");
        return model;
    }

    @RequestMapping(value = "/quanhuyens/addNewQuanHuyen", method = RequestMethod.POST)
    public ModelAndView doAddNew(HttpServletRequest httpServletRequest, @ModelAttribute("quanhuyenForm") QuanHuyenForm quanhuyenForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", quanhuyenForm.getName());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.containsKey("message")) {
                    ModelAndView view = new ModelAndView("quanhuyenForm");
                    view.addObject("quanhuyenForm", quanhuyenForm);
                    view.addObject("error", object.get("message"));
                    return view;
                } else {
                    ModelAndView view = new ModelAndView("redirect:/quanhuyens");
                    view.addObject("message", "Thêm quận huyện thành công");
                    return view;
                }

            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/quanhuyens/editQuanHuyen/{id}", method = RequestMethod.POST)
    public ModelAndView doEditQuanHuyen(HttpServletRequest httpServletRequest, @PathVariable("id") Long id, @ModelAttribute("quanhuyenForm") QuanHuyenForm quanhuyenForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", quanhuyenForm.getName());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            stringEntity.setContentType("application/json");
            stringEntity.setContentEncoding("UTF-8");
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                ModelAndView view = new ModelAndView();
                if (!object.containsKey("message")) {
                    view.addObject("message", "Sửa quận huyện thành công");
                    view = new ModelAndView("redirect:/quanhuyens");
                    return view;
                } else {
                    view.addObject("error", object.get("message"));
                    view = new ModelAndView("redirect:/quanhuyens/editQuanHuyen/{id}");
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/quanhuyens/editQuanHuyen/{id}", method = RequestMethod.GET)
    public ModelAndView editQuanHuyen(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.size() > 0) {
                    if (object.containsKey("message")) {
                        ModelAndView view = new ModelAndView("index");
                        String message = "Không tìm thấy dữ liệu";
                        view.addObject("message", message);
                        return view;
                    } else {
                        JSONObject jsonObject = object;
                        QuanHuyenForm quanhuyenForm = new QuanHuyenForm();
                        quanhuyenForm.setId((Long) jsonObject.get("id"));
                        quanhuyenForm.setName((String) jsonObject.get("name"));
                        ModelAndView view = new ModelAndView("quanhuyenForm");
                        view.addObject("quanhuyenForm", quanhuyenForm);
                        return view;
                    }
                } else {
                    return null;
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
