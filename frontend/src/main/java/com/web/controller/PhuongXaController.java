package com.web.controller;

import com.web.bean.AuthTokenInfo;
import com.web.dto.PhuongXaDTO;
import com.web.dto.QuanHuyenDTO;
import com.web.model.PhuongXaForm;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/14/2017.
 */
@PropertySource("classpath:url.properties")
@Controller
public class PhuongXaController {

    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    private AuthTokenInfo tokenInfo;

    @RequestMapping(value = "/phuongxas", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView getAllPhuongXas(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/phuongxa");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
//                JSONObject object = parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    ModelAndView view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<PhuongXaDTO> phuongxas = new ArrayList<>();
                    PhuongXaDTO phuongxaDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        phuongxaDTO = new PhuongXaDTO();
                        phuongxaDTO.setId((long) jsonObject.get("id"));
                        phuongxaDTO.setName((String) jsonObject.get("name"));
                        phuongxaDTO.setNameQuanHuyen((String) jsonObject.get("nameQuanHuyen"));
                        phuongxaDTO.setQuantityBTS(Integer.parseInt(jsonObject.get("quantityBTS").toString()));
                        phuongxas.add(phuongxaDTO);
                    }
                    ModelAndView view = new ModelAndView("managePhuongXas");
                    view.addObject("phuongxas", phuongxas);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/phuongxas/addNewPhuongXa", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView addNewPhuongXa(HttpServletRequest httpServletRequest, ModelAndView view) {
        PhuongXaForm phuongxaForm = new PhuongXaForm();
        view.addObject("phuongxaForm", phuongxaForm);
        view.setViewName("phuongxaForm");
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<QuanHuyenDTO> quanhuyens = new ArrayList<>();
                    QuanHuyenDTO quanhuyenDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        quanhuyenDTO = new QuanHuyenDTO();
                        quanhuyenDTO.setId((long) jsonObject.get("id"));
                        quanhuyenDTO.setName((String) jsonObject.get("name"));
                        quanhuyens.add(quanhuyenDTO);
                    }
                    view.addObject("quanhuyens", quanhuyens);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value = "/phuongxas/addNewPhuongXa", method = RequestMethod.POST)
    public ModelAndView doAddNew(HttpServletRequest httpServletRequest, @ModelAttribute("phuongxaForm") PhuongXaForm phuongxaForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/phuongxa");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", phuongxaForm.getName());
            jsonObject.put("idQuanHuyen", phuongxaForm.getIdQuanHuyen());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.containsKey("message")) {
                    ModelAndView view = new ModelAndView("phuongxaForm");
                    view.addObject("phuongxaForm", phuongxaForm);
                    view.addObject("error", object.get("message"));
                    return view;
                } else {
                    ModelAndView view = new ModelAndView("redirect:/phuongxas");
                    view.addObject("message", "Thêm phường xã thành công");
                    return view;
                }

            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/phuongxas/editPhuongXa/{id}", method = RequestMethod.POST)
    public ModelAndView doEditPhuongXa(HttpServletRequest httpServletRequest, @PathVariable("id") Long id, @ModelAttribute("phuongxaForm") PhuongXaForm phuongxaForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/phuongxa/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", phuongxaForm.getName());
            jsonObject.put("idQuanHuyen", phuongxaForm.getIdQuanHuyen());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            stringEntity.setContentType("application/json");
            stringEntity.setContentEncoding("UTF-8");
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                ModelAndView view = new ModelAndView();
                if (!object.containsKey("message")) {
                    view.addObject("message", "Sửa phường xã thành công");
                    view = new ModelAndView("redirect:/phuongxas");
                    return view;
                } else {
                    view.addObject("error", object.get("message"));
                    view = new ModelAndView("redirect:/phuongxas/editPhuongXa/{id}");
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/phuongxas/editPhuongXa/{id}", method = RequestMethod.GET)
    public ModelAndView editPhuongXa(HttpServletRequest httpServletRequest, @PathVariable("id") Long id, ModelAndView view) {
        PhuongXaForm phuongxaForm = new PhuongXaForm();
        view.addObject("phuongxaForm", phuongxaForm);
        view.setViewName("phuongxaForm");
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder1 = new URIBuilder(REST_SERVICE_URI + "api/quanhuyen");
            builder1.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri1 = builder1.build();
            HttpGet request1 = new HttpGet(uri1);
            HttpResponse response1 = httpClient.execute(request1);
            HttpEntity entity1 = response1.getEntity();
            if (entity1 != null) {
                String json = EntityUtils.toString(entity1);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<QuanHuyenDTO> quanhuyens = new ArrayList<>();
                    QuanHuyenDTO quanhuyenDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        quanhuyenDTO = new QuanHuyenDTO();
                        quanhuyenDTO.setId((long) jsonObject.get("id"));
                        quanhuyenDTO.setName((String) jsonObject.get("name"));
                        quanhuyens.add(quanhuyenDTO);
                    }
                    view.addObject("quanhuyens", quanhuyens);

                    URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/phuongxa/" + id);
                    builder.setParameter("access_token", tokenInfo.getAccess_token());
                    URI uri = builder.build();
                    HttpGet request = new HttpGet(uri);
                    HttpResponse response = httpClient.execute(request);
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        String json1 = EntityUtils.toString(entity);
                        JSONObject object1 = (JSONObject) parser.parse(json1);
                        if (object1.size() > 0) {
                            if (object1.containsKey("message")) {
                                view = new ModelAndView("index");
                                String message = "Không tìm thấy dữ liệu";
                                view.addObject("message", message);
                                return view;
                            } else {
//                                JSONObject jsonObject1 = object1;
                                phuongxaForm.setId((Long) object1.get("id"));
                                phuongxaForm.setName((String) object1.get("name"));
                                phuongxaForm.setIdQuanHuyen((Long) object1.get("idQuanHuyen"));
                                view.addObject("phuongxaForm", phuongxaForm);
                                return view;
                            }
                        } else {
                            return null;
                        }
                    }
                    return view;
                }
            } else {
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
