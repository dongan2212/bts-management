package com.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.bean.AuthTokenInfo;
import com.web.dto.AccountDTO;
import com.web.model.AccountForm;
import com.web.model.LoginForm;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 10/14/2017.
 */
@PropertySource("classpath:url.properties")
@Controller
public class AccountController {

    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    private AuthTokenInfo tokenInfo;

    /*
     * Prepare HTTP Headers.
     */
    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

    /*
     * Add HTTP Authorization header, using Basic-Authentication to send client-credentials.
     */
    private String getHeadersWithClientCredentials() {
        String plainClientCredentials = "my-trusted-client:secret";
        String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));

//        HttpHeaders headers = getHeaders();
//        headers.add("Authorization", "Basic " + base64ClientCredentials);
        return base64ClientCredentials;
    }

    @SuppressWarnings({"unchecked"})
    private AuthTokenInfo sendTokenRequest(String username, String password) {

        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(AUTH_SERVER_URI);
            builder.setParameter("grant_type", "password");
            builder.setParameter("username", username);
            builder.setParameter("password", password);

            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            request.setHeader("Authorization", "Basic " + getHeadersWithClientCredentials());

            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(json);
                if (jsonObject.size() > 2) {
                    AuthTokenInfo tokenInfo = new AuthTokenInfo();
                    tokenInfo.setAccess_token((String) jsonObject.get("access_token"));
                    tokenInfo.setToken_type((String) jsonObject.get("token_type"));
                    tokenInfo.setRefresh_token((String) jsonObject.get("refresh_token"));
                    tokenInfo.setExpires_in((Long) jsonObject.get("expires_in"));
                    tokenInfo.setScope((String) jsonObject.get("scope"));
                    return tokenInfo;
                } else {
                    return null;
                }
            } else {
                return null;
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView getAllUsers(HttpServletRequest httpServletRequest) {
        HttpSession httpSession = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) httpSession.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/user");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    ModelAndView view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<AccountDTO> accountDTOS = new ArrayList<>();
                    AccountDTO accountDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        accountDTO = new AccountDTO();
                        accountDTO.setName((String) jsonObject.get("name"));
                        accountDTO.setUsername((String) jsonObject.get("username"));
                        accountDTO.setEmail((String) jsonObject.get("email"));
                        accountDTOS.add(accountDTO);
                    }
                    ModelAndView view = new ModelAndView("manageAccounts");
                    view.addObject("users", accountDTOS);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/users/addNewAccount", method = RequestMethod.GET)
    public ModelAndView addNewAccount(ModelAndView model) {
        AccountForm accountForm = new AccountForm();
        model.addObject("newAccountForm", accountForm);
        model.setViewName("newAccountForm");
        return model;
    }

    @RequestMapping(value = "/users/addNewAccount", method = RequestMethod.POST)
    public ModelAndView doAddNew(HttpServletRequest httpServletRequest, @ModelAttribute("accountForm") AccountForm accountForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/user");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", accountForm.getName());
            jsonObject.put("email", accountForm.getEmail());
            jsonObject.put("username", accountForm.getUsername());
            jsonObject.put("password", accountForm.getPassword());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.containsKey("message")) {
                    ModelAndView view = new ModelAndView("newAccountForm");
                    view.addObject("newAccountForm", accountForm);
                    view.addObject("error", object.get("message"));
                    return view;
                } else {
                    ModelAndView view = new ModelAndView("redirect:/users");
                    view.addObject("message", "Thêm tài khoản thành công");
                    return view;
                }

            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView view = new ModelAndView("login");
        LoginForm loginForm = new LoginForm();
        view.addObject("loginForm", loginForm);
        return view;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "text/html; charset=UTF-8")
    public @ResponseBody
    String doLogin(HttpServletRequest httpServletRequest, @ModelAttribute("loginForm") LoginForm loginForm) throws JsonProcessingException {
        tokenInfo = sendTokenRequest(loginForm.getUsername(), loginForm.getPassword());
        if (tokenInfo == null) {
            ModelAndView view = new ModelAndView("login");
            view.addObject("loginForm", loginForm);
            view.addObject("error", "Tên đăng nhập hoặc mật khẩu không đúng!");
            ObjectMapper mapper = new ObjectMapper();
            String result = mapper.writeValueAsString("<script>$(document).ready(function () {$.simplyToast" + "('Tên hoặc mật khẩu không đúng!', " + "'danger');});</script>");
            return result;
        } else {
            HttpSession session = httpServletRequest.getSession();
            session.setAttribute("username", loginForm.getUsername());
            session.setAttribute("token", tokenInfo);
            HttpClient httpClient = HttpClients.createDefault();
            try {
                URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/user/username/" + loginForm.getUsername());
                builder.setParameter("access_token", tokenInfo.getAccess_token());
                URI uri = builder.build();
                HttpGet request = new HttpGet(uri);
                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String json = EntityUtils.toString(entity);
                    JSONParser parser = new JSONParser();
                    JSONObject object = (JSONObject) parser.parse(json);
                    AccountDTO accountDTO = new AccountDTO();
                    accountDTO.setId((Long) object.get("id"));
                    accountDTO.setEmail((String) object.get("email"));
                    accountDTO.setUsername(loginForm.getUsername());
                    accountDTO.setName((String) object.get("name"));
                    session.setAttribute("accountDTO", accountDTO);
                }
                ObjectMapper mapper = new ObjectMapper();
                String result = mapper.writeValueAsString("<script>$( location ).attr(\"href\", \"/\");" + "</script>");
                return result;
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString("<script>$(document).ready(function () {$.simplyToast" + "('Đăng nhập không thành công!', " + "'danger');});</script>");
        return result;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView doLogout(HttpServletRequest httpServletRequest) throws JsonProcessingException {
        HttpSession session = httpServletRequest.getSession();
        session.invalidate();
        ModelAndView view = new ModelAndView("login");
        view.addObject("message", "Đăng xuất thành công");
        return view;
    }

    @RequestMapping(value = "/userprofile", method = RequestMethod.GET)
    public ModelAndView showUserProfile(HttpServletRequest servletRequest, @ModelAttribute("accountForm") AccountForm accountForm) {
        HttpSession session = servletRequest.getSession();
        AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDTO");
        if (accountDTO == null || tokenInfo == null) {
            ModelAndView view = new ModelAndView("index");
            String message = "Bạn chưa đăng nhập!";
            view.addObject("error", message);
            return view;
        } else {
            ModelAndView view = new ModelAndView("user_profile");
            view.addObject("accountDTO", accountDTO);
            return view;
        }
    }

    @RequestMapping(value = "/updateaccount", method = RequestMethod.POST)
    public ModelAndView updateAccount(HttpServletRequest httpServletRequest, @ModelAttribute("accountForm") AccountForm accountForm) {
        HttpSession session = httpServletRequest.getSession();
        AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDTO");
        if (accountDTO == null || tokenInfo == null) {
            ModelAndView view = new ModelAndView("index");
            String message = "Bạn chưa đăng nhập!";
            view.addObject("error", message);
            return view;
        } else {
            HttpClient httpClient = HttpClients.createDefault();
            try {
                URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/user/" + accountDTO.getId() + "/" + accountDTO.getUsername());
                builder.setParameter("access_token", tokenInfo.getAccess_token());
                URI uri = builder.build();
                HttpPost request = new HttpPost(uri);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", accountForm.getName());
                jsonObject.put("email", accountForm.getEmail());
                jsonObject.put("username", accountForm.getUsername());
                jsonObject.put("password", accountForm.getPassword());
                jsonObject.put("id", accountForm.getId());
                StringEntity stringEntity = new StringEntity(jsonObject.toString());
                request.setHeader("content-type", "application/json");
                request.setEntity(stringEntity);
                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String json = EntityUtils.toString(entity);
                    JSONParser parser = new JSONParser();
                    JSONObject object = (JSONObject) parser.parse(json);
                    if (object.size() > 0) {
                        if (object.containsKey("message")) {
                            ModelAndView view = new ModelAndView("redirect:/userprofile");
                            view.addObject("accountForm", accountForm);
                            view.addObject("error", object.get("message").toString());
                            return view;
                        } else {
                            accountDTO = new AccountDTO();
                            accountDTO.setEmail((String) jsonObject.get("email"));
                            accountDTO.setName((String) jsonObject.get("name"));
                            accountDTO.setPassword((String) jsonObject.get("password"));
                            accountDTO.setId(Long.parseLong(jsonObject.get("id").toString()));
                            accountDTO.setUsername((String) jsonObject.get("username"));
                            session.setAttribute("accountDTO", accountDTO);
                            ModelAndView view = new ModelAndView("user_profile");
                            view.addObject("accountDTO", accountDTO);
                            view.addObject("message", "Cập nhật thành công!");
                            return view;
                        }
                    }
                } else {
                    return null;
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}