package com.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.bean.AuthTokenInfo;
import com.web.dto.BtsDTO;
import com.web.dto.CompanyDTO;
import com.web.dto.PhuongXaDTO;
import com.web.model.BtsForm;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/14/2017.
 */
@PropertySource("classpath:url.properties")
@Controller
public class BTSController {
    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    private AuthTokenInfo tokenInfo;

    @RequestMapping(value = "/detailbts/{id}", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public String getBtsById(HttpServletRequest httpServletRequest, @PathVariable("id") int idBts) {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts/" + idBts);
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.size() > 0) {
                    if (object.containsKey("message")) {
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString(object.get("message"));
                        return result;
                    } else {
                        JSONObject jsonObject;
                        List<BtsDTO> btss = new ArrayList<>();
                        BtsDTO btsDTO;
                        jsonObject = object;
                        btsDTO = new BtsDTO();
                        btsDTO.setId((Long) jsonObject.get("id"));
                        btsDTO.setName((String) jsonObject.get("name"));
                        btsDTO.setAddress((String) jsonObject.get("address"));
                        btsDTO.setHeight((Double) jsonObject.get("height"));
                        btsDTO.setNamePhuongXa((String) jsonObject.get("namePhuongXa"));
                        btsDTO.setNameQuanHuyen((String) jsonObject.get("nameQuanHuyen"));
                        btsDTO.setNameOwner((String) jsonObject.get("nameOwner"));
                        btsDTO.setNameUser((String) jsonObject.get("nameUser"));
                        btsDTO.setNote((String) jsonObject.get("note"));
                        btsDTO.setInformation((String) jsonObject.get("information"));
                        btsDTO.setLongtitude((Double) jsonObject.get("longtitude"));
                        btsDTO.setLatitude((Double) jsonObject.get("latitude"));
                        btsDTO.setMarkerIconBTS((String) jsonObject.get("markerIconBTS"));
                        btss.add(btsDTO);
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString(btss);
                        return result;
                    }
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/btss-load", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    public
    @ResponseBody
    String showAllMarker() {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts");
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return initForMap(entity);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (ParseException e) {
//            e.printStackTrace();
//        }
        return null;
    }

    @RequestMapping(value = "/companies/mapdata/{id}", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    public
    @ResponseBody
    String showBTSOfCompanyId(@PathVariable("id") Long id) {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts/companyowner/" + id);
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return initForMap(entity);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/companies/show-map/{id}", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    public
    @ResponseBody
    ModelAndView showMappByCompanyId(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("mapByCompanyId");
        view.addObject("idComp", id);
        return view;
    }

    @RequestMapping(value = "/btss", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView getAllBtss() {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts");
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
//                JSONObject object = parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    ModelAndView view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<BtsDTO> btss = new ArrayList<>();
                    BtsDTO btsDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        btsDTO = new BtsDTO();
                        btsDTO.setId((Long) jsonObject.get("id"));
                        btsDTO.setName((String) jsonObject.get("name"));
                        btsDTO.setAddress((String) jsonObject.get("address"));
                        btsDTO.setNamePhuongXa((String) jsonObject.get("namePhuongXa"));
                        btsDTO.setNameQuanHuyen((String) jsonObject.get("nameQuanHuyen"));
                        btsDTO.setNameOwner((String) jsonObject.get("nameOwner"));
                        btsDTO.setNameUser((String) jsonObject.get("nameUser"));
                        btsDTO.setNameCreator((String) jsonObject.get("nameCreator"));
                        btss.add(btsDTO);
                    }
                    ModelAndView view = new ModelAndView("manageBtss");
                    view.addObject("btss", btss);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/btss/deleteBts/{id}", method = RequestMethod.GET)
    public ModelAndView deleteBts(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        HttpSession httpSession = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) httpSession.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpDelete request = new HttpDelete(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            ModelAndView view = new ModelAndView("redirect:/btss");
            if (entity == null) {
                view.addObject("message", "Xóa trạm BTS thành công");
                return view;
            } else {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                view.addObject("error", object.get("message"));
                return view;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/btss/addNewBts", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView addNewBts(HttpServletRequest httpServletRequest, ModelAndView view) {
        BtsForm btsForm = new BtsForm();
        view.addObject("btsForm", btsForm);
        view.setViewName("btsForm");
        initForPhuongXa(httpServletRequest, view);
        initForCompany(httpServletRequest, view);
        return view;
    }

    @RequestMapping(value = "/btss/addNewBts", method = RequestMethod.POST)
    public ModelAndView doAddNew(HttpServletRequest httpServletRequest, @ModelAttribute("btsForm") BtsForm btsForm) {
        HttpSession httpSession = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) httpSession.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", btsForm.getName());
            jsonObject.put("idPhuongXa", btsForm.getIdPhuongXa());
            jsonObject.put("idUser", btsForm.getIdUser());
            jsonObject.put("idOwner", btsForm.getIdOwner());
            jsonObject.put("note", btsForm.getNote());
            jsonObject.put("information", btsForm.getInformation());
            jsonObject.put("status", btsForm.getStatus());
            jsonObject.put("height", btsForm.getHeight());
            jsonObject.put("latitude", btsForm.getLatitude());
            jsonObject.put("longtitude", btsForm.getLongtitude());
            jsonObject.put("address", btsForm.getAddress());
            jsonObject.put("nameCreator", httpSession.getAttribute("username"));
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.containsKey("message")) {
                    ModelAndView view = new ModelAndView("btsForm");
                    view.addObject("btsForm", btsForm);
                    view.addObject("error", object.get("message"));
                    return view;
                } else {
                    ModelAndView view = new ModelAndView("redirect:/btss");
                    view.addObject("message", "Thêm trạm BTS thành công");
                    return view;
                }

            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/btss/editBts/{id}", method = RequestMethod.POST)
    public ModelAndView doEditBts(HttpServletRequest httpServletRequest, @PathVariable("id") Long id, @ModelAttribute("btsForm") BtsForm btsForm) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", btsForm.getName());
            jsonObject.put("address", btsForm.getAddress());
            jsonObject.put("note", btsForm.getNote());
            jsonObject.put("height", btsForm.getHeight());
            jsonObject.put("information", btsForm.getInformation());
            jsonObject.put("idOwner", btsForm.getIdOwner());
            jsonObject.put("idUser", btsForm.getIdUser());
            jsonObject.put("nameCreator", session.getAttribute("username"));
            jsonObject.put("status", btsForm.getStatus());
            jsonObject.put("longtitude", btsForm.getLongtitude());
            jsonObject.put("latitude", btsForm.getLatitude());
            jsonObject.put("idPhuongXa", btsForm.getIdPhuongXa());
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            stringEntity.setContentType("application/json");
            stringEntity.setContentEncoding("UTF-8");
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                ModelAndView view = new ModelAndView();
                if (!object.containsKey("message")) {
                    view.addObject("message", "Sửa thông tin trạm BTS thành công");
                    view = new ModelAndView("redirect:/btss");
                    return view;
                } else {
                    view.addObject("error", object.get("message"));
                    view = new ModelAndView("redirect:/btss/editBts/{id}");
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/btss/editBts/{id}", method = RequestMethod.GET)
    public ModelAndView editBts(HttpServletRequest httpServletRequest, @PathVariable("id") Long id, ModelAndView view) {
        BtsForm btsForm = new BtsForm();
        view.addObject("btsForm", btsForm);
        view.setViewName("btsForm");
        initForPhuongXa(httpServletRequest, view);
        initForCompany(httpServletRequest, view);
        HttpSession httpSession = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) httpSession.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/bts/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.isEmpty()) {
                    view = new ModelAndView("manageBtss");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    btsForm.setId((Long) object.get("id"));
                    btsForm.setName((String) object.get("name"));
                    btsForm.setAddress((String) object.get("address"));
                    btsForm.setHeight(Double.parseDouble(object.get("height").toString()));
                    btsForm.setLatitude(Double.parseDouble(object.get("latitude").toString()));
                    btsForm.setLongtitude(Double.parseDouble(object.get("longtitude").toString()));
                    btsForm.setNote((String) object.get("note"));
                    btsForm.setInformation((String) object.get("information"));
                    btsForm.setStatus(Integer.parseInt(object.get("status").toString()));
                    btsForm.setIdPhuongXa((Long) object.get("idPhuongXa"));
                    btsForm.setIdOwner((Long) object.get("idOwner"));
                    btsForm.setIdUser((Long) object.get("idUser"));
                    btsForm.setUserCreated((String) object.get("nameCreator"));
                    view.addObject("btsForm", btsForm);
                    return view;
                }
            } else {
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<BtsDTO> parseFromJSONToDTOMap(JSONArray jsonArray) {
        List<BtsDTO> btsDTOS = new ArrayList<>();
        BtsDTO btsDTO;
        JSONObject jsonObject;
        for (int i = 0; i < jsonArray.size(); i++) {
            jsonObject = (JSONObject) jsonArray.get(i);
            btsDTO = new BtsDTO();
            btsDTO.setId((Long) jsonObject.get("id"));
            btsDTO.setName((String) jsonObject.get("name"));
            btsDTO.setAddress((String) jsonObject.get("address"));
            btsDTO.setHeight((Double) jsonObject.get("height"));
            btsDTO.setNamePhuongXa((String) jsonObject.get("namePhuongXa"));
            btsDTO.setNameQuanHuyen((String) jsonObject.get("nameQuanHuyen"));
            btsDTO.setNameOwner((String) jsonObject.get("nameOwner"));
            btsDTO.setNameUser((String) jsonObject.get("nameUser"));
            btsDTO.setNote((String) jsonObject.get("note"));
            btsDTO.setInformation((String) jsonObject.get("information"));
            btsDTO.setLongtitude((Double) jsonObject.get("longtitude"));
            btsDTO.setLatitude((Double) jsonObject.get("latitude"));
            btsDTO.setMarkerIconBTS((String) jsonObject.get("markerIconBTS"));
            btsDTO.setIdOwner((Long) jsonObject.get("idOwner"));
            btsDTOS.add(btsDTO);
        }
        return btsDTOS;
    }

    public String initForMap(HttpEntity entity) {
        try {
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    return null;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    ObjectMapper mapper = new ObjectMapper();
                    String result = mapper.writeValueAsString(parseFromJSONToDTOMap(jsonArray));
                    return result;
                }
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void initForPhuongXa(HttpServletRequest httpServletRequest, ModelAndView view) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/phuongxa");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    view = new ModelAndView("btsForm");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                } else {
                    List<PhuongXaDTO> phuongxaDTOS = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    PhuongXaDTO phuongxaDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        phuongxaDTO = new PhuongXaDTO();
                        phuongxaDTO.setId((Long) jsonObject.get("id"));
                        phuongxaDTO.setName((String) jsonObject.get("name"));
                        phuongxaDTOS.add(phuongxaDTO);
                    }
                    view.addObject("phuongxas", phuongxaDTOS);
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initForCompany(HttpServletRequest httpServletRequest, ModelAndView view) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            String json = EntityUtils.toString(entity);
            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray) parser.parse(json);
            JSONObject object = new JSONObject();
            object.put("listComp", array);
            if (object.isEmpty()) {
                view = new ModelAndView("btsForm");
                String message = "Không tìm thấy dữ liệu";
                view.addObject("message", message);
            } else {
                JSONArray jsonArray = (JSONArray) object.get("listComp");
                List<CompanyDTO> companyDTOS = new ArrayList<>();
                CompanyDTO companyDTO;
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    companyDTO = new CompanyDTO();
                    companyDTO.setId((Long) jsonObject.get("id"));
                    companyDTO.setName((String) jsonObject.get("name"));
                    companyDTOS.add(companyDTO);
                }
                view.addObject("companyDTOS", companyDTOS);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
