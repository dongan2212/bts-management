package com.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.bean.AuthTokenInfo;
import com.web.dto.CompanyDTO;
import com.web.model.CompanyForm;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/14/2017.
 */
@PropertySource("classpath:url.properties")
@Controller
public class CompanyController {
    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    @Value("${image.path}")
    private String UPLOADED_FOLDER;

    private AuthTokenInfo tokenInfo;

    @RequestMapping(value = "/companies", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public ModelAndView getAllCompanies() {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company");
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
//                JSONObject object = parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    ModelAndView view = new ModelAndView("index");
                    String message = "Không tìm thấy dữ liệu";
                    view.addObject("message", message);
                    return view;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<CompanyDTO> companies = new ArrayList<>();
                    CompanyDTO companyDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        companyDTO = new CompanyDTO();
                        companyDTO.setId((long) jsonObject.get("id"));
                        companyDTO.setName((String) jsonObject.get("name"));
                        companyDTO.setMarkericon((String) jsonObject.get("markericon"));
                        companyDTO.setEmail((String) jsonObject.get("email"));
                        companyDTO.setPhone((String) jsonObject.get("phone"));
                        companyDTO.setNote((String) jsonObject.get("note"));
                        companyDTO.setQuantityBTSOwner(Integer.parseInt(jsonObject.get("quantityBTSOwner").toString()));
                        companyDTO.setQuantityBTSUser(Integer.parseInt(jsonObject.get("quantityBTSUser").toString()));
                        companies.add(companyDTO);
                    }
                    ModelAndView view = new ModelAndView("manageCompanies");
                    view.addObject("companies", companies);
                    return view;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/company-numbts", method = RequestMethod.GET, produces = "text/html; charset=UTF-8")
    @ResponseBody
    public String showCompanyMenu() {
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company");
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONArray array = (JSONArray) parser.parse(json);
                JSONObject object = new JSONObject();
                object.put("list", array);
                if (object.isEmpty()) {
                    return null;
                } else {
                    JSONArray jsonArray = (JSONArray) object.get("list");
                    JSONObject jsonObject;
                    List<CompanyDTO> companies = new ArrayList<>();
                    CompanyDTO companyDTO;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        companyDTO = new CompanyDTO();
                        companyDTO.setId((long) jsonObject.get("id"));
                        companyDTO.setName((String) jsonObject.get("name"));
                        companyDTO.setMarkericon((String) jsonObject.get("markericon"));
                        companyDTO.setEmail((String) jsonObject.get("email"));
                        companyDTO.setPhone((String) jsonObject.get("phone"));
                        companyDTO.setNote((String) jsonObject.get("note"));
                        companyDTO.setQuantityBTSOwner(Integer.parseInt(jsonObject.get("quantityBTSOwner").toString()));
                        companyDTO.setQuantityBTSUser(Integer.parseInt(jsonObject.get("quantityBTSUser").toString()));
                        companies.add(companyDTO);
                    }
                    ObjectMapper mapper = new ObjectMapper();
                    String result = mapper.writeValueAsString(companies);
                    return result;
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/companies/addNewCompany", method = RequestMethod.GET)
    public ModelAndView addNewCompany(ModelAndView model) {
        CompanyForm companyForm = new CompanyForm();
        model.addObject("companyForm", companyForm);
        model.setViewName("companyForm");
        return model;
    }

    @RequestMapping(value = "/companies/addNewCompany", method = RequestMethod.POST)
    public @ResponseBody String doAddNew(HttpServletRequest httpServletRequest,
                                 @RequestParam("id") Long idComp,
                                 @RequestParam("name") String name,
                                 @RequestParam("phone") String phone,
                                 @RequestParam("email") String email,
                                 @RequestParam("note") String note,
                                 @RequestParam("file") MultipartFile image) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company");
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("phone", phone);
            jsonObject.put("markericon", "/markers/" + image.getOriginalFilename());
            jsonObject.put("note", note);
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.size() > 0) {
                    if (object.containsKey("message")) {
                        String message = (String) object.get("message");
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString(message);
                        return result;
                    } else {
                        byte[] imageData = image.getBytes();
                        Path path = Paths.get(UPLOADED_FOLDER + image.getOriginalFilename());
                        Files.write(path, imageData);
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString("Thêm công ty thành công");
                        return result;
                    }
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/companies/editCompany/{id}",
            headers = "content-type=multipart/*",
            method = RequestMethod.POST,
            produces = "text/html; charset=UTF-8",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String doEditCompany(HttpServletRequest httpServletRequest,
                                      @PathVariable("id") Long id,
                                      @RequestParam("file") MultipartFile image,
                                      @RequestParam("id") Long idComp,
                                      @RequestParam("name") String name,
                                      @RequestParam("phone") String phone,
                                      @RequestParam("email") String email,
                                      @RequestParam("note") String note) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("markericon", "/markers/" + image.getOriginalFilename());
            jsonObject.put("id", idComp);
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("phone", phone);
            jsonObject.put("note", note);
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            stringEntity.setContentType("application/json");
            stringEntity.setContentEncoding("UTF-8");
            request.setHeader("content-type", "application/json");
            request.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.size() > 0) {
                    if (object.containsKey("message")) {
                        String message = (String) object.get("message");
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString(message);
                        return result;
                    } else {
                        byte[] imageData = image.getBytes();
                        Path path = Paths.get(UPLOADED_FOLDER + image.getOriginalFilename());
                        Files.write(path, imageData);
                        ObjectMapper mapper = new ObjectMapper();
                        String result = mapper.writeValueAsString("Cập nhật thành công");
                        return result;
                    }
                }
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/companies/editCompany/{id}", method = RequestMethod.GET)
    public ModelAndView editCompany(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        HttpSession session = httpServletRequest.getSession();
        tokenInfo = (AuthTokenInfo) session.getAttribute("token");
        HttpClient httpClient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder(REST_SERVICE_URI + "api/company/" + id);
            builder.setParameter("access_token", tokenInfo.getAccess_token());
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity);
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(json);
                if (object.size() > 0) {
                    if (object.containsKey("message")) {
                        ModelAndView view = new ModelAndView("index");
                        String message = "Không tìm thấy dữ liệu";
                        view.addObject("message", message);
                        return view;
                    } else {
                        JSONObject jsonObject = object;
                        CompanyForm companyForm = new CompanyForm();
                        companyForm.setId((Long) jsonObject.get("id"));
                        companyForm.setName((String) jsonObject.get("name"));
                        companyForm.setEmail((String) jsonObject.get("email"));
                        companyForm.setPhone((String) jsonObject.get("phone"));
                        companyForm.setNote((String) jsonObject.get("note"));
                        companyForm.setMarkericon((String) jsonObject.get("markericon"));
                        ModelAndView view = new ModelAndView("companyForm");
                        view.addObject("companyForm", companyForm);
                        return view;
                    }
                } else {
                    return null;
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}
