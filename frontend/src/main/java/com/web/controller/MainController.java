package com.web.controller;

import com.web.bean.AuthTokenInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Administrator on 10/16/2017.
 */

@PropertySource("classpath:url.properties")
@Controller
public class MainController {
    @Value("${url.REST_SERVICE_URI}")
    private String REST_SERVICE_URI;

    @Value("${url.AUTH_SERVER_URI}")
    private String AUTH_SERVER_URI;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showMap() {
        return "index";
    }
}
