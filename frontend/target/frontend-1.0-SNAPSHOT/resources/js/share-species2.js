/**
 * Created by duyle on 4/13/17.
 */

$(document).ajaxStart(function () {
    $.LoadingOverlay("show");
});

$(document).ajaxStop(function () {
    $.LoadingOverlay("hide");
});

function showMap() {
    $('#myModal').modal('show');
}

function readURL(input) {
    var nimeType = input.files[0]['type'];
    console.log("typ: " + nimeType);
    if (nimeType.split('/')[0] == 'image') {
        $("#btnSubmitForm").prop('disabled', false);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $(document).ready(function () {
            $("#btnSubmitForm").prop('disabled', true);
            $.simplyToast('File được chọn không phải hình!', 'danger');
        });
    }
}

function submitFormCompany() {
    $("body").off("load");
    var idCom = this.id.value;
    console.log("text: " + this.id.value);
    if ($('#markericon').val() == "" || $('#markericon').val() == null) {
        $(document).ready(function () {
            $.simplyToast('Bạn chưa chọn hình!', 'danger');
        });
    } else if (this.id.value) {
        var myForm = new FormData();
        myForm.append("file", $('#markericon')[0].files[0]);
        var form_data = $('#form-data').serialize();
        $.ajax({
            type: "POST",
            url: window.location.origin + "/companies/editCompany/" + idCom + "?" + form_data,
            data: myForm,
            encoding: "UTF-8",
            processData: false,
            contentType: false,
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                var regex = new RegExp("Cập nhật thành công");
                var result = "";
                if (regex.test(data) == true) {
                    result = "<script>" + "window.location.replace(\"/companies\");" +
                        "$(document).ready(function () " +
                        "{$.simplyToast('Cập nhật thành công!', 'success');" +
                        "}); " + "</script>";
                    $("#result").html(result);
                } else {
                    var notification = data;
                    result = "<script>" +
                        "$(document).ready(function () " +
                        "{$.simplyToast('" + notification + "', 'danger');" +
                        "}); " + "</script>";
                    $("#result").html(result);
                }
            },
            error: function (e) {
                $(document).ready(function () {
                    $.simplyToast('Cập nhật không thành công', 'danger');
                });
            }
        });
    } else {
        var myForm = new FormData();
        myForm.append("file", $('#markericon')[0].files[0]);
        var form_data = $('#form-data').serialize();
        $.ajax({
            type: "POST",
            url: window.location.origin + "/companies/addNewCompany?" + form_data,
            data: myForm,
            encoding: "UTF-8",
            processData: false,
            contentType: false,
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                var regex = new RegExp("Thêm công ty thành công");
                var result = "";
                if (regex.test(data) == true) {
                    result = "<script>" + "window.location.replace(\"/companies\");" +
                        "$(document).ready(function () " +
                        "{$.simplyToast('Thêm công ty thành công!', 'success');" +
                        "}); " + "</script>";
                    $("#result").html(result);
                } else {
                    var notification = data;
                    result = "<script>" +
                        "$(document).ready(function () " +
                        "{$.simplyToast('" + notification + "', 'danger');" +
                        "}); " + "</script>";
                    $("#result").html(result);
                }
            },
            error: function (e) {
                $(document).ready(function () {
                    $.simplyToast('Thêm công ty không thành công', 'danger');
                });
            }
        });
    }
}