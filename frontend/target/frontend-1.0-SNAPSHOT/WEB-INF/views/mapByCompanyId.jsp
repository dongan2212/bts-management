<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 10/10/2017
  Time: 3:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="../jspf/header.jspf" %>
<link href="<c:url value="/resources/css/index.css"/>" rel="stylesheet"/>
<!-- Bibliothèque de base: Leaflet-->
<link href="<c:url value="/resources/plugins/leaflet/leaflet.css"/>" rel="stylesheet">
<!-- Draw-->
<link href="<c:url value="/resources/plugins/leaflet/leaflet.draw.css"/>" rel="stylesheet">

<!-- Slide menu-->
<link href="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.css"/>" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- GeoCoder-->
<link href="<c:url value="/resources/plugins/leaflet/Control.OSMGeocoder.css"/>" rel="stylesheet">

<!-- Overview-->
<link href="<c:url value="/resources/plugins/leaflet/overview/MiniMap.css"/>" rel="stylesheet">

<!-- Localisation-->
<link href="<c:url value="/resources/plugins/leaflet/L.Control.Locate.min.css"/>" rel="stylesheet">

<!-- Mouse position-->
<link href="<c:url value="/resources/plugins/leaflet/L.Control.MousePosition.css"/>" rel="stylesheet">

<!-- Navigation Bar-->
<link href="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.css"/>" rel="stylesheet">

<body>
<div class="wrapper">
    <%@include file="../jspf/slider.jspf" %>
    <div class="main-panel ps-container ps-theme-default ps-active-y">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Bản đồ</a>
                </div>
                <div class="collapse navbar-collapse">
                    <%@include file="../jspf/navbar-right.jspf" %>
                </div>
            </div>
        </nav>
        <div id="map" style="min-height: 100%"></div>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: -133.667px;">
            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>

    </div>
</div>

<div id="myModal" class="modal modal-wide fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>
            <div class="modal-body" align="center" id="modal-body">

            </div>
        </div>
    </div>
</div>
</body>

<%@include file="../jspf/footer.jspf" %>
<!-- Bibliothèque de base: Leaflet-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.js"/>" type="text/javascript"></script>
<!-- Draw-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw-src.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw.js"/>" type="text/javascript"></script>

<!-- Slide menu-->
<script src="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.js"/>" type="text/javascript"></script>
<!-- GeoCoder-->
<script src="<c:url value="/resources/plugins/leaflet/Control.OSMGeocoder.js"/>" type="text/javascript"></script>

<!-- Overview-->
<script src="<c:url value="/resources/plugins/leaflet/overview/MiniMap.js"/>" type="text/javascript"></script>

<!-- Localisation-->
<script src="<c:url value="/resources/plugins/leaflet/L.Control.Locate.js"/>" type="text/javascript"></script>

<!-- Mouse position-->
<script src="<c:url value="/resources/plugins/leaflet/L.Control.MousePosition.js"/>"
        type="text/javascript"></script>

<!-- Navigation Bar-->
<script src="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resources/js/geojson.min.js"/>" type="text/javascript"></script>

<script>
    var GoogleStreet = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga');
    var googleSat = L.tileLayer('https://mt0.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}&s=Ga');
    var OpenStreetMap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    ///// Configuration de la map
    var map = L.map('map', {
        layers: [GoogleStreet], /// fond de base
        center: new L.LatLng(16.65549, 105.38635),/// coordonnées
        crs: L.CRS.EPSG3857,
        tms: true,
        minZoom: 1,
        maxZoom: 20,
        zoom: 9
    });

    /////layers de base
    var baseLayers = {
        "Google Street": GoogleStreet,
        "Google Sat": googleSat,
        "OpenStreetMap": OpenStreetMap
    };

    ///// Add the scale control to the map
    L.control.scale().addTo(map);

    ///// Add the geolocate control to the map
    L.control.locate({
        position: 'topleft',  // set the location of the control
        drawCircle: true,  // controls whether a circle is drawn that shows the uncertainty about the location
        follow: false,  // follow the user's location
        setView: true, // automatically sets the map view to the user's location, enabled if `follow` is true
        keepCurrentZoomLevel: false, // keep the current map zoom level when displaying the user's location. (if `false`, use maxZoom)
        stopFollowingOnDrag: false, // stop following when the map is dragged if `follow` is true (deprecated, see below)
        remainActive: false, // if true locate control remains active on click even if the user's location is in view.
        markerClass: L.circleMarker, // L.circleMarker or L.marker
        circleStyle: {},  // change the style of the circle around the user's location
        markerStyle: {},
        followCircleStyle: {},  // set difference for the style of the circle around the user's location while following
        followMarkerStyle: {},
        icon: 'fa fa-map-marker',  // class for icon, fa-location-arrow or fa-map-marker
        iconLoading: 'fa fa-spinner fa-spin',  // class for loading icon
        circlePadding: [0, 0], // padding around accuracy circle, value is passed to setBounds
        metric: true,  // use metric or imperial units
        onLocationError: function (err) {
            alert(err.message)
        },  // define an error callback function
        onLocationOutsideMapBounds: function (context) { // called when outside map boundaries
            alert(context.options.strings.outsideMapBoundsMsg);
        },
        showPopup: true, // display a popup when the user click on the inner marker
        strings: {
            title: "Show me where I am",  // title of the locate control
            popup: "You are within {distance} {unit} from this point",  // text to appear if user clicks on circle
            outsideMapBoundsMsg: "You seem located outside the boundaries of the map" // default message for onLocationOutsideMapBounds
        },
        locateOptions: {}  // define location options e.g enableHighAccuracy: true or maxZoom: 10
    }).addTo(map);

    ///// Add the mouse position to the map
    L.control.mousePosition().addTo(map);

    ///// Ajout des couches de base + couches geoserver
    L.control.layers(baseLayers).addTo(map);
    //    overlays.natural.addTo(map);

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=UTF-8",
        url: "${home}/companies/mapdata/" +${idComp},
        encoding: "UTF-8",
        dataType: 'json',
        timeout: 100000,
        success: function (data) {
            var geo = GeoJSON.parse(data, {Point: ['latitude', 'longtitude']});
            console.log("SUCCESS: " + geo);
            handleJson(geo);
        },
    });

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=UTF-8",
        url: "${home}/company-numbts",
        encoding: "UTF-8",
        dataType: 'json',
        timeout: 100000,
        success: function (data) {
            for (var key in data) {
                var comp = data[key];
                var urlComp = '<c:url value="/companies/show-map/"/>' + comp.id;
                $('#compbtss').append("<li id=" + comp.id + "><a href=" + urlComp + ">" + "<p>Nhà mạng " + comp.name + " ( " + comp.quantityBTSOwner + " )</p></a></li>");
            }
        }
    });

    var group = new L.featureGroup().addTo(map);
    var geojsonlayer;
    function handleJson(data) {
        geojsonlayer = L.geoJson(data, {
            onEachFeature: function (feature, my_Layer) {
                my_Layer.on('click', function (e) {
                    show(feature["properties"].id);
                });
            },
            pointToLayer: function (feature, latlng) {
                console.log("marker: " + feature["properties"].markerIconBTS);
                var markerIcon = new L.Icon({
                    iconUrl: feature["properties"].markerIconBTS,
                    shadowUrl: '/resources/img/markers/marker-shadow.png',
                    iconSize: [25, 41],
                    iconAnchor: [12, 41],
                    popupAnchor: [1, -34],
                    shadowSize: [41, 41]
                });
                return L.marker(latlng, {icon: markerIcon});
            }
        }).addTo(group);
    }

</script>
<c:if test="${error != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${error}"/>', 'danger');
        });
    </script>
</c:if>
<c:if test="${message != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${message}"/>', 'success');
        });
    </script>
</c:if>
<script>
    function show(id) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=UTF-8",
            url: "${home}/detailbts/" + id,
            encoding: "UTF-8",
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                console.log("SUCCESS: ", data);
                var geo = GeoJSON.parse(data, {Point: 'longtitude,latitude'});
                console.log("SUCCESS: ", geo);
                if (data != null) {
                    var result = "";
                    for (var key in data) {
                        var obj = data[key];
                        var title = obj.name + ' (' + obj.latitude + ' - ' + obj.longtitude + ")";
                        $('#modal-title').html(title);
                        result += "<class=\"w3-left w3-margin-right\"></a>"
                            + "<div align=\"left\">"
                            + "<p class=\"vietnamese-name\">Tên trạm BTS: " + obj.name + "</p>"
                            + "<p class=\"science-name\">Địa chỉ: " + obj.address + "</p>"
                            + "<p class=\"science-name\">Chủ sở hữu: " + obj.nameOwner + "</p>"
                            + "<p class=\"science-name\">Quận/Huyện: " + obj.nameQuanHuyen + "</p>"
                            + "<p class=\"science-name\">Phường/Xã: " + obj.namePhuongXa + "</p>"
                            + "<p class=\"science-name\">Đang sử dụng: " + obj.nameUser + "</p>"
                            + "<p class=\"science-name\">Thông tin: " + obj.information + "</p>"
                            + "</div>"
                            + "</div>"
                            + "</div>";
                    }
                    $("#modal-body").html(result);
                    $('#myModal').modal('show');
                } else {
                    $(document).ready(function () {
                        $.simplyToast('Không tìm thấy dữ liệu', 'danger');
                    });
                }
            },
            error: function (e) {
                $(document).ready(function () {
                    $.simplyToast('Không tìm thấy dữ liệu', 'danger');
                });
                console.log("ERROR: ", e);
            }
        });
    }
</script>
<script src="<c:url value="/resources/js/index.js"/>" type="text/javascript"></script>
</html>
