<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 10/22/2017
  Time: 12:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<%@include file="../jspf/header.jspf" %>

<link href="<c:url value="/resources/css/index.css"/>" rel="stylesheet"/>
<link href="<c:url value="/resources/css/table.css"/>" rel="stylesheet"/>

<!-- Slide menu-->
<link href="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.css"/>" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Navigation Bar-->
<link href="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.css"/>" rel="stylesheet">
<style>
    @media (min-width: 992px) {
        .card form [class*="col-"]:first-child {
            padding: 6px;
        }
    }

</style>
<body>
<div class="wrapper">
    <%@include file="../jspf/slider.jspf" %>
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Bản đồ</a>
                </div>
                <div class="collapse navbar-collapse">
                    <%@include file="../jspf/navbar-right.jspf" %>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Thêm/Sửa Thông tin Trạm BTS</h4>
                            </div>
                            <div class="content">
                                <form:form method="post" commandName="btsForm">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class=" col-md-4" hidden="hidden">
                                                                <form:label path="id" hidden="true">
                                                                    ID
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8" hidden>
                                                                <form:hidden path="id"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="name">
                                                                    Tên Trạm BTS:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="name"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="address">
                                                                    Địa chỉ:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="address"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="information">
                                                                    Thông tin trạm:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="information"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="note">
                                                                    Chú thích:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="note"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="longtitude">
                                                                    Kinh độ:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="longtitude"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="latitude">
                                                                    Vĩ độ:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="latitude"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="height">
                                                                    Chiều cao:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="height"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="status">
                                                                    Trạng thái:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:input path="status" type="number"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="idPhuongXa">
                                                                    Phường/Xã:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:select path="idPhuongXa"
                                                                             data-error="Phải chọn phường/xã!!">
                                                                    <form:option value="0" label="Chọn phường/xã"/>
                                                                    <c:if test="${phuongxas != null}"/>
                                                                    <c:forEach var="obj" items="${phuongxas}">
                                                                        <form:option value="${obj.id}"
                                                                                     label="${obj.name}"/>
                                                                    </c:forEach>
                                                                </form:select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="idOwner">
                                                                    Chủ sở hữu:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:select path="idOwner"
                                                                             data-error="Phải chọn công ty sở hữu!!">
                                                                    <form:option value="0" label="Chọn công ty sở hữu"/>
                                                                    <c:if test="${companyDTOS != null}"/>
                                                                    <c:forEach var="obj" items="${companyDTOS}">
                                                                        <form:option value="${obj.id}"
                                                                                     label="${obj.name}"/>
                                                                    </c:forEach>
                                                                </form:select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <form:label path="idUser">
                                                                    Đang sử dụng:
                                                                </form:label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <form:select path="idUser"
                                                                             data-error="Phải chọn công ty đang sử dụng!!">
                                                                    <form:option value="0" label="Chọn công ty đang sử dụng"/>
                                                                    <c:if test="${companyDTOS != null}"/>
                                                                    <c:forEach var="obj" items="${companyDTOS}">
                                                                        <form:option value="${obj.id}"
                                                                                     label="${obj.name}"/>
                                                                    </c:forEach>
                                                                </form:select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-fill pull-right">Save
                                    </button>
                                    <div class="clearfix"></div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<%@include file="../jspf/footer.jspf" %>
<!-- Bibliothèque de base: Leaflet-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.js"/>" type="text/javascript"></script>
<!-- Draw-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw-src.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw.js"/>" type="text/javascript"></script>

<!-- Slide menu-->
<script src="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.js"/>" type="text/javascript"></script>
<!-- Navigation Bar-->
<script src="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resources/js/index.js"/>" type="text/javascript"></script>
<script>
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=UTF-8",
        url: "${home}/company-numbts",
        encoding: "UTF-8",
        dataType: 'json',
        timeout: 100000,
        success: function (data) {
            for (var key in data) {
                var comp = data[key];
                var urlComp = '<c:url value="/companies/show-map/"/>' + comp.id;
                $('#compbtss').append("<li id=" + comp.id + "><a href=" + urlComp + ">" + "<p>Nhà mạng " + comp.name + " ( " + comp.quantityBTSOwner + " )</p></a></li>");
            }
        }
    });
</script>
<c:if test="${error != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${error}"/>', 'danger');
        });
    </script>
</c:if>
<c:if test="${message != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${message}"/>', 'success');
        });
    </script>
</c:if>
</html>
