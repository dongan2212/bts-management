<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 10/10/2017
  Time: 3:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="../jspf/header.jspf" %>

<link href="<c:url value="/resources/css/index.css"/>" rel="stylesheet"/>
<link href="<c:url value="/resources/css/table.css"/>" rel="stylesheet"/>

<!-- Slide menu-->
<link href="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.css"/>" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Navigation Bar-->
<link href="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.css"/>" rel="stylesheet">
<body>
<div class="wrapper">
    <%@include file="../jspf/slider.jspf" %>
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Bản đồ</a>
                </div>
                <div class="collapse navbar-collapse">
                    <%@include file="../jspf/navbar-right.jspf" %>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header" style="text-align: center">
                                <h4 class="title">Danh sách Nhà Mạng</h4>
                                <h3>
                                    <a href="/companies/addNewCompany" class="btn btn-primary">Thêm
                                        mới</a>
                                </h3>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead style="text-align: center">
                                    <th>Tên</th>
                                    <th>Email</th>
                                    <th>Điện thoại</th>
                                    <th>Số trạm BTS</th>
                                    <th>Chú thích</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="company" items="${companies}">
                                        <tr>
                                            <td>${company.name}</td>
                                            <td>${company.email}</td>
                                            <td>${company.phone}</td>
                                            <td>${company.quantityBTSOwner}</td>
                                            <td>${company.note}</td>
                                            <td>
                                                <a href="/companies/editCompany/${company.id}">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<%@include file="../jspf/footer.jspf" %>
<!-- Bibliothèque de base: Leaflet-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.js"/>" type="text/javascript"></script>
<!-- Draw-->
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw-src.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/plugins/leaflet/leaflet.draw.js"/>" type="text/javascript"></script>

<!-- Slide menu-->
<script src="<c:url value="/resources/plugins/leaflet/slide_menu/SlideMenu.js"/>" type="text/javascript"></script>
<!-- Navigation Bar-->
<script src="<c:url value="/resources/plugins/leaflet/NavBar/NavBar.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resources/js/index.js"/>" type="text/javascript"></script>
<script>
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=UTF-8",
        url: "${home}/company-numbts",
        encoding: "UTF-8",
        dataType: 'json',
        timeout: 100000,
        success: function (data) {
            for (var key in data) {
                var comp = data[key];
                var urlComp = '<c:url value="/companies/show-map/"/>' + comp.id;
                $('#compbtss').append("<li id=" + comp.id + "><a href=" + urlComp + ">" + "<p>Nhà mạng " + comp.name + " ( " + comp.quantityBTSOwner + " )</p></a></li>");
            }
        }
    });
</script>
<c:if test="${error != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${error}"/>', 'danger');
        });
    </script>
</c:if>
<c:if test="${message != null}">
    <script>
        $(document).ready(function () {
            $.simplyToast('<c:out value="${message}"/>', 'success');
        });
    </script>
</c:if>
</html>
