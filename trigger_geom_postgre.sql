﻿-- trigger function
CREATE OR REPLACE FUNCTION fn_bts_geo_update_event() RETURNS trigger AS $fn_bts_geo_update_event$
  BEGIN  
	-- as this is an after trigger, NEW contains all the information we need even for INSERT
	UPDATE bts SET 
	geom = ST_Transform(ST_SetSRID(ST_MakePoint(NEW.longtitude,NEW.latitude), 4326),4326)
	WHERE id=NEW.id;
    RETURN NULL; -- result is ignored since this is an AFTER trigger
  END;
$fn_bts_geo_update_event$ LANGUAGE plpgsql;

-- triggers
-- INSERT trigger
DROP TRIGGER IF EXISTS tr_bts_inserted ON bts;
CREATE TRIGGER tr_bts_inserted
  AFTER INSERT ON bts
  FOR EACH ROW
  EXECUTE PROCEDURE fn_bts_geo_update_event();


 --  UPDATE trigger
DROP TRIGGER IF EXISTS tr_bts_geo_updated ON bts;
CREATE TRIGGER tr_bts_geo_updated
  AFTER UPDATE OF 
  latitude,
  longtitude
  ON bts
  FOR EACH ROW
  EXECUTE PROCEDURE fn_bts_geo_update_event();
  
-- test queries
--INSERT INTO bts (latitude, longtitude) VALUES(43.653226, -79.3831843);
--UPDATE bts SET latitude=39.653226 WHERE id=1;
--SELECT to_timestamp(updated_ts), * FROM bts;