package com.web.repository;

import com.web.entity.QuanHuyen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 6/10/2017.
 */
@Repository
public interface QuanHuyenRepository extends CrudRepository<QuanHuyen, Long> {


    @Query(value = "SELECT * FROM quanhuyen ORDER BY name ASC", nativeQuery = true)
    List<QuanHuyen> getAllOrderbyName();

    @Query(value = "SELECT * FROM quanhuyen WHERE name = :name LIMIT 1", nativeQuery = true)
    QuanHuyen isItemExistByName(@Param("name") String name);

    @Query(value = "SELECT * FROM quanhuyen WHERE id <> :id AND name = :name LIMIT 1", nativeQuery = true)
    QuanHuyen isItemExistByIdName(@Param("id") Long id,@Param("name") String name);


}
