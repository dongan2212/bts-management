package com.web.repository;

import com.web.entity.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Bi on 6/10/2017.
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    @Query(value = "SELECT * FROM company WHERE name = :name OR markericon = :markericon LIMIT 1", nativeQuery = true)
    Company isItemExistByNameOrMarker(@Param("name") String name, @Param("markericon") String markericon);

    @Query(value = "SELECT * FROM company WHERE id <> :id AND (name = :name OR markericon = :markericon) LIMIT 1", nativeQuery = true)
    Company isItemExistByIdNameOrMarker(@Param("id") Long id, @Param("name") String name, @Param("markericon") String markericon);
}
