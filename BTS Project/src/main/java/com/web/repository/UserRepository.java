package com.web.repository;

import com.web.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Bi on 6/10/2017.
 */

public interface UserRepository extends JpaRepository<User, Long > {

    User findByUsername(String username);

    @Query(value = "SELECT * FROM users WHERE id = :id AND username = :username LIMIT 1", nativeQuery = true)
    User findByIdUsername(@Param("id") Long Id,@Param("username") String username);
}

