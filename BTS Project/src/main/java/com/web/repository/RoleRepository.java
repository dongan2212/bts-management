//package com.web.repository;
//
//import com.web.entity.Role;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
///**
// * Created by Bi on 6/10/2017.
// */
//@Repository
//public interface RoleRepository extends CrudRepository<Role, Long> {
//    Role findByName(String name);
//}
