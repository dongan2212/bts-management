package com.web.repository;

import com.web.entity.PhuongXa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 6/10/2017.
 */
@Repository
public interface PhuongXaRepository extends CrudRepository<PhuongXa, Long> {


    @Query(value = "SELECT * FROM phuongxa ORDER BY idquanhuyen ASC, name ASC", nativeQuery = true)
    List<PhuongXa> getAllOrderByIdQuanHuyenName ();

    @Query(value = "SELECT * FROM phuongxa WHERE idquanhuyen = :idquanhuyen ORDER BY name ASC", nativeQuery = true)
    List<PhuongXa> getListByIdQuanHuyen (@Param("idquanhuyen") Long idquanhuyen);

    @Query(value = "SELECT COUNT(*) FROM phuongxa INNER JOIN quanhuyen ON quanhuyen.id = phuongxa.idquanhuyen\n" +
            "WHERE quanhuyen.id = :idquanhuyen", nativeQuery = true)
    int countPhuongXaByIdQuanHuyen(@Param("idquanhuyen") Long idquanhuyen);

    @Query(value = "SELECT * FROM phuongxa WHERE name = :name LIMIT 1", nativeQuery = true)
    PhuongXa isItemExistByName(@Param("name") String name);

    @Query(value = "SELECT * FROM phuongxa WHERE idquanhuyen = :idquanhuyen AND name = :name LIMIT 1", nativeQuery = true)
    PhuongXa isItemExistByIdQuanHuyenName(@Param("idquanhuyen") Long idquanhuyen,@Param("name") String name);


}
