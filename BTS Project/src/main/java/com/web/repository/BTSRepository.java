package com.web.repository;

import com.web.entity.BTS;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 6/10/2017.
 */
@Repository
public interface BTSRepository extends CrudRepository<BTS, Long> {
    @Query(value = "SELECT * FROM bts WHERE name = :name LIMIT 1", nativeQuery = true)
    BTS isItemExistByName(@Param("name") String name);

    @Query(value = "SELECT * FROM bts WHERE id <> :id  AND name = :name  LIMIT 1", nativeQuery = true)
    BTS isItemExistByIdName(@Param("id") Long id, @Param("name") String name);

    @Query(value = "SELECT * FROM bts WHERE iduser = :iduser ", nativeQuery = true)
    List<BTS> getListByIdUser(@Param("iduser") Long iduser);

    @Query(value = "SELECT COUNT(*) FROM bts INNER JOIN company ON bts.iduser = company.id\n" +
            "WHERE company.id = :idcompany", nativeQuery = true)
    int countBTSByIdUser(@Param("idcompany") Long idcompany);

    @Query(value = "SELECT * FROM bts WHERE idowner = :idowner ", nativeQuery = true)
    List<BTS> getListByIdOwner(@Param("idowner") Long idowner);

    @Query(value = "SELECT COUNT(*) FROM bts INNER JOIN company ON bts.idowner = company.id\n" +
            "WHERE company.id = :idcompany", nativeQuery = true)
    int countBTSByIdOwner(@Param("idcompany") Long idcompany);

    @Query(value = "SELECT * FROM bts WHERE idphuong = :idphuongxa ", nativeQuery = true)
    List<BTS> getListByIdPhuongXa(@Param("idphuongxa") Long idphuongxa);

    @Query(value = "SELECT COUNT(*) FROM bts INNER JOIN phuongxa ON bts.idphuong = phuongxa.id\n" +
            "WHERE phuongxa.id = :idphuongxa", nativeQuery = true)
    int countBTSByIdPhuongXa(@Param("idphuongxa") Long idphuongxa);

    @Query(value = "SELECT bts.* FROM bts INNER JOIN phuongxa ON bts.idphuong = phuongxa.id INNER JOIN quanhuyen" +
            " ON phuongxa.idquanhuyen = quanhuyen.id WHERE idquanhuyen = :idquanhuyen ", nativeQuery = true)
    List<BTS> getListByIdQuanHuyen(@Param("idquanhuyen") Long idquanhuyen);

    @Query(value = "SELECT count(*) FROM bts INNER JOIN phuongxa ON bts.idphuong = phuongxa.id INNER JOIN quanhuyen\n" +
            "ON phuongxa.idquanhuyen = quanhuyen.id WHERE idquanhuyen = :idquanhuyen", nativeQuery = true)
    int countBTSByIdQuanHuyen(@Param("idquanhuyen") Long idquanhuyen);
}
