package com.web.repository;

import com.web.entity.Member;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Bi on 5/11/2017.
 */
public interface MemberRepository extends CrudRepository<Member, Long>{


    Member findByUsername(String username);

    @Query(value = "SELECT * FROM member WHERE id = :id AND username = :username LIMIT 1", nativeQuery = true)
    Member findByIdUsername(@Param("id") Long Id, @Param("username") String username);
}
