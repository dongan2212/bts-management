package com.web.service;

import com.web.dto.UserMemberDTO;
import com.web.entity.Member;
import com.web.entity.User;

import java.util.List;

/**
 * Created by Bi on 29/10/2017.
 */
public interface UserService {


    List<UserMemberDTO> getAllItem();

    UserMemberDTO getItemById(Long id);

    UserMemberDTO getItemByUsername(String username);

    UserMemberDTO addItem(UserMemberDTO itemDTO);

    UserMemberDTO updateItem(Long id, UserMemberDTO itemChange, String userlogin);

    boolean isItemExistByItem(UserMemberDTO itemDTO);
}
