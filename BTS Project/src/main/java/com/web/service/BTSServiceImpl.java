package com.web.service;

import com.web.dto.BtsDTO;
import com.web.entity.*;
import com.web.repository.*;
import com.web.common.CurrentDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 8/10/2017.
 */
@Service
public class BTSServiceImpl implements BTSService {

    @Autowired
    private BTSRepository btsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private PhuongXaRepository phuongXaRepository;
    @Autowired
    private QuanHuyenRepository quanHuyenRepository;
    private BtsDTO transferEntitytoDTO(BTS item){
        BtsDTO itemDTO = new BtsDTO();
        itemDTO.setId(item.getId());
        itemDTO.setName(item.getName());
        itemDTO.setAddress(item.getAddress());
        itemDTO.setLongtitude(item.getLongtitude());
        itemDTO.setLatitude(item.getLatitude());
        itemDTO.setHeight(item.getHeight());
        itemDTO.setStatus(item.getStatus());
        itemDTO.setInformation(item.getInformation());
        itemDTO.setNote(item.getNote());
        itemDTO.setIdUserCreated(item.getUserCreated().getId());
        itemDTO.setIdOwner(item.getIdOwner().getId());
        itemDTO.setIdUser(item.getIdUser().getId());
        itemDTO.setNameCreator(item.getUserCreated().getName());
        itemDTO.setNameOwner(item.getIdOwner().getName());
        itemDTO.setNameUser(item.getIdUser().getName());
        itemDTO.setDateCreated(item.getDateCreated());
        itemDTO.setDateUpdated(item.getDateUpdated());
        itemDTO.setMarkerIconBTS(item.getIdUser().getMarkericon());
        itemDTO.setIdPhuongXa(item.getIdPhuongXa().getId());
        itemDTO.setNamePhuongXa(item.getIdPhuongXa().getName());
        itemDTO.setIdQuanHuyen(item.getIdPhuongXa().getIdQuanHuyen().getId());
        itemDTO.setNameQuanHuyen(item.getIdPhuongXa().getIdQuanHuyen().getName());
        // Số lượng trạm BTS
        return itemDTO;
    }


    private BTS transferDTOtoEntity(BtsDTO itemDTO){
        BTS item = new BTS();
        item.setId(itemDTO.getId());
        item.setName(itemDTO.getName());
        item.setAddress(itemDTO.getAddress());
        item.setLongtitude(itemDTO.getLongtitude());
        item.setLatitude(itemDTO.getLatitude());
        item.setHeight(itemDTO.getHeight());
        item.setStatus(itemDTO.getStatus());
        item.setInformation(itemDTO.getInformation());
        item.setNote(itemDTO.getNote());
        Member userCreated = memberRepository.findByUsername(itemDTO.getNameCreator());
        item.setUserCreated(userCreated);
        Company idOwner = companyRepository.findOne(itemDTO.getIdOwner());
        item.setIdOwner(idOwner);
        Company idUser = companyRepository.findOne(itemDTO.getIdUser());
        item.setIdUser(idUser);
        PhuongXa phuongXa = phuongXaRepository.findOne(itemDTO.getIdPhuongXa());
        item.setIdPhuongXa(phuongXa);
        item.setDateCreated(itemDTO.getDateCreated());
        item.setDateUpdated(itemDTO.getDateUpdated());
        return item;
    }
    
    
    @Override
    public List<BtsDTO> getAllItem() {
        List<BTS> list = new ArrayList<>();
        btsRepository.findAll().forEach(list::add);
        List<BtsDTO> listDTO = new ArrayList<>();
        for(BTS item: list){
            BtsDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public BtsDTO getItemById(Long id) {
        BTS item = btsRepository.findOne(id);
        BtsDTO itemDTO = transferEntitytoDTO(item);
        return itemDTO;
    }

    @Override
    public BtsDTO addItem(BtsDTO itemDTO) {

        itemDTO.setDateCreated(CurrentDate.getCurrentSQLDate());
        BTS item = transferDTOtoEntity(itemDTO);
        BTS done = btsRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public BtsDTO updateItem(Long id, BtsDTO itemDTO) {
        itemDTO.setId(id);
        BTS temp = btsRepository.findOne(id);
        itemDTO.setDateUpdated(CurrentDate.getCurrentSQLDate());
        BTS item = transferDTOtoEntity(itemDTO);
        item.setDateCreated(temp.getDateCreated());
        BTS done = btsRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public boolean isItemExistByItem(BtsDTO itemDTO) {
        BTS item = null;
        if(itemDTO.getId() != null){
            item = btsRepository.isItemExistByIdName(itemDTO.getId(), itemDTO.getName());
        }
        else{
            item = btsRepository.isItemExistByName(itemDTO.getName());
        }
        if(item != null) return true;
        return false;
    }

    @Override
    public List<BtsDTO> getItemByIdQuanHuyen(Long id) {

        List<BTS> list = btsRepository.getListByIdQuanHuyen(id);
        List<BtsDTO> listDTO = new ArrayList<>();
        for(BTS item: list){
            BtsDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public List<BtsDTO> getItemByIdPhuongXa(Long id) {
        List<BTS> list = btsRepository.getListByIdPhuongXa(id);
        List<BtsDTO> listDTO = new ArrayList<>();
        for(BTS item: list){
            BtsDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public List<BtsDTO> getItemByIdUser(Long id) {
        List<BTS> list = btsRepository.getListByIdUser(id);
        List<BtsDTO> listDTO = new ArrayList<>();
        for(BTS item: list){
            BtsDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public List<BtsDTO> getItemByIdOwner(Long id) {
        List<BTS> list = btsRepository.getListByIdOwner(id);
        List<BtsDTO> listDTO = new ArrayList<>();
        for(BTS item: list){
            BtsDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public void deleteItem(Long id) {
        BTS item = btsRepository.findOne(id);
        btsRepository.delete(item);
    }
}



