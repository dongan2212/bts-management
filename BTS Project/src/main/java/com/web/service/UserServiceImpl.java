package com.web.service;

import com.web.dto.UserMemberDTO;
import com.web.entity.Member;
import com.web.entity.User;
import com.web.repository.MemberRepository;
import com.web.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 24/10/2017.
 */
@Service
public class UserServiceImpl  implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MemberRepository memberRepository;

    private UserMemberDTO transferEntitytoDTO(Member item){
        UserMemberDTO itemDTO = new UserMemberDTO();
        itemDTO.setId(item.getId());
        itemDTO.setUsername(item.getUsername());
        itemDTO.setName(item.getName());
        itemDTO.setEmail(item.getEmail());
        return itemDTO;
    }

    private User addDataUser(UserMemberDTO userMemberDTO){
        User user = new User();
        user.setUsername(userMemberDTO.getUsername());
        user.setPassword(getPasswordEncoder().encode(userMemberDTO.getPassword()));
        return user;
    }

    private Member addDataMember(UserMemberDTO userMemberDTO){
        Member member = new Member();
        member.setUsername(userMemberDTO.getUsername());
        member.setName(userMemberDTO.getName());
        member.setEmail(userMemberDTO.getEmail());
        return member;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public List<UserMemberDTO> getAllItem() {
        List<Member> list = new ArrayList<>();
        memberRepository.findAll().forEach(list::add);
        List<UserMemberDTO> listDTO = new ArrayList<>();
        for(Member item: list){
            UserMemberDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;

    }

    @Override
    public UserMemberDTO getItemById(Long id) {
        Member member = memberRepository.findOne(id);
        UserMemberDTO itemDTO = null;
        if(member !=null) itemDTO = transferEntitytoDTO(member);
        return itemDTO;

    }

    @Override
    public UserMemberDTO getItemByUsername(String username) {
        Member member = memberRepository.findByUsername(username);
        UserMemberDTO itemDTO = null;
        if(member !=null) itemDTO = transferEntitytoDTO(member);
        return itemDTO;
    }

    @Override
    public UserMemberDTO  addItem(UserMemberDTO userMemberDTO) {
        User user = addDataUser(userMemberDTO);
        Member member = addDataMember(userMemberDTO);
        userRepository.save(user);
        member = memberRepository.save(member);

        UserMemberDTO itemDTO = transferEntitytoDTO(member);
        return itemDTO;
    }

    @Override
    public UserMemberDTO updateItem(Long id, UserMemberDTO itemChange, String userlogin) {
        User userBean = null;
        Member memberBean = null;
        if(userlogin.equals(itemChange.getUsername()) && id == itemChange.getId()){
            userBean = userRepository.findByUsername(userlogin);
            memberBean = memberRepository.findByIdUsername(id,userlogin);
            if(userBean != null && memberBean != null){
                if(!itemChange.getPassword().equals("") && itemChange.getPassword() != null){
                    userBean.setPassword(getPasswordEncoder().encode(itemChange.getPassword()));
                }
                memberBean.setEmail(itemChange.getEmail());
                memberBean.setName(itemChange.getName());
                userBean = userRepository.save(userBean);
                memberBean = memberRepository.save(memberBean);
            }
        }
        UserMemberDTO itemDTO = null;
        if(memberBean!= null) itemDTO = transferEntitytoDTO(memberBean);
        return itemDTO;

    }

    @Override
    public boolean isItemExistByItem(UserMemberDTO userMemberDTO) {
        User userItem = userRepository.findByUsername(userMemberDTO.getUsername());;
        Member memberItem = memberRepository.findByUsername(userMemberDTO.getUsername());;
        if(userItem != null && memberItem!=null) return true;
        return false;
    }
}
