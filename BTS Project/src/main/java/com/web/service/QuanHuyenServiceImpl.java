    package com.web.service;

    import com.web.dto.QuanHuyenDTO;
    import com.web.entity.QuanHuyen;
    import com.web.repository.BTSRepository;
    import com.web.repository.PhuongXaRepository;
    import com.web.repository.QuanHuyenRepository;
    import com.web.common.CurrentDate;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.stereotype.Service;

    import java.util.ArrayList;
    import java.util.List;

    /**
     * Created by Bi on 6/10/2017.
     */
    @Service
    public class QuanHuyenServiceImpl implements QuanHuyenService {

        @Autowired
        private QuanHuyenRepository quanHuyenRepository;
        @Autowired
        private PhuongXaRepository phuongXaRepository;
        @Autowired
        private BTSRepository btsRepository;
        private QuanHuyenDTO transferEntitytoDTO(QuanHuyen item){
            QuanHuyenDTO itemDTO = new QuanHuyenDTO();
            itemDTO.setId(item.getId());
            itemDTO.setName(item.getName());
            itemDTO.setDateCreated(item.getDateCreated());
            itemDTO.setDateUpdated(item.getDateUpdated());
            itemDTO.setQuantityPhuongXa(phuongXaRepository.countPhuongXaByIdQuanHuyen(item.getId()));
            itemDTO.setQuantityBTS(btsRepository.countBTSByIdQuanHuyen(item.getId()));
            //Số lượng phường, xã // Số lượng trạm BTS
            return itemDTO;
        }


        private QuanHuyen transferDTOtoEntity(QuanHuyenDTO itemDTO){
            QuanHuyen item = new QuanHuyen();
            item.setId(itemDTO.getId());
            item.setName(itemDTO.getName());
            item.setDateCreated(itemDTO.getDateCreated());
            item.setDateUpdated(itemDTO.getDateUpdated());

            return item;
        }

        @Override
        public List<QuanHuyenDTO> getAllItem() {
            List<QuanHuyen> list = quanHuyenRepository.getAllOrderbyName();
            List<QuanHuyenDTO> listDTO = new ArrayList<>();
            for(QuanHuyen item: list){
                QuanHuyenDTO itemDTO = transferEntitytoDTO(item);
                listDTO.add(itemDTO);
            }
            return listDTO;
        }

        @Override
        public QuanHuyenDTO getItemById(Long id) {
            QuanHuyen item = quanHuyenRepository.findOne(id);
            QuanHuyenDTO itemDTO = transferEntitytoDTO(item);
            return itemDTO;
        }



        @Override
        public QuanHuyenDTO addItem(QuanHuyenDTO itemDTO) {
            itemDTO.setDateCreated(CurrentDate.getCurrentSQLDate());
            QuanHuyen item = transferDTOtoEntity(itemDTO);

            QuanHuyen done = quanHuyenRepository.save(item);
            itemDTO = transferEntitytoDTO(done);
            return itemDTO;
        }

        @Override
        public QuanHuyenDTO updateItem(Long id, QuanHuyenDTO itemDTO ) {
            itemDTO.setId(id);
            QuanHuyen temp = quanHuyenRepository.findOne(id);
            itemDTO.setDateUpdated(CurrentDate.getCurrentSQLDate());
            QuanHuyen item = transferDTOtoEntity(itemDTO);
            item.setDateCreated(temp.getDateCreated());
            QuanHuyen done = quanHuyenRepository.save(item);
            itemDTO = transferEntitytoDTO(done);
            return itemDTO;
        }

        @Override
        public boolean isItemExistByItem(QuanHuyenDTO itemDTO) {
            QuanHuyen item = null;
            if(itemDTO.getId() != null){
                item = quanHuyenRepository.isItemExistByIdName(itemDTO.getId(), itemDTO.getName());
            }
            else{
                item = quanHuyenRepository.isItemExistByName(itemDTO.getName());
            }
            if(item != null) return true;
            return false;
        }
    }
