package com.web.service;

import com.web.dto.CompanyDTO;

import java.util.List;

/**
 * Created by Bi on 7/10/2017.
 */
public interface CompanyService {

    List<CompanyDTO> getAllItem();

    CompanyDTO getItemById(Long id);

    CompanyDTO addItem(CompanyDTO itemDTO);

    CompanyDTO updateItem(Long id, CompanyDTO itemDTO);

    boolean isItemExistByItem(CompanyDTO itemDTO);

    //List<CompanyDTO> getClassByPhylumId(Long id);
}
