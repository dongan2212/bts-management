package com.web.service;

import com.web.dto.BtsDTO;
import com.web.dto.PhuongXaDTO;

import java.util.List;

/**
 * Created by Bi on 8/10/2017.
 */
public interface BTSService {

    List<BtsDTO> getAllItem();

    BtsDTO getItemById(Long id);

    BtsDTO addItem(BtsDTO itemDTO);

    BtsDTO updateItem(Long id, BtsDTO itemDTO);

    boolean isItemExistByItem(BtsDTO itemDTO);

    List<BtsDTO> getItemByIdQuanHuyen(Long id);

    List<BtsDTO> getItemByIdPhuongXa(Long id);

    List<BtsDTO> getItemByIdUser(Long id);

    List<BtsDTO> getItemByIdOwner(Long id);

    void deleteItem(Long id);

}
