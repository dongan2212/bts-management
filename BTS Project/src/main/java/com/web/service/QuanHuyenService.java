package com.web.service;

import com.web.dto.QuanHuyenDTO;

import java.util.List;

/**
 * Created by Bi on 6/10/2017.
 */
public interface QuanHuyenService {
    List<QuanHuyenDTO> getAllItem();

    QuanHuyenDTO getItemById(Long id);

    QuanHuyenDTO addItem(QuanHuyenDTO itemDTO);

    QuanHuyenDTO updateItem(Long id, QuanHuyenDTO itemDTO);

    boolean isItemExistByItem(QuanHuyenDTO itemDTO);


}
