package com.web.service;

import com.web.dto.PhuongXaDTO;
import com.web.dto.QuanHuyenDTO;

import java.util.List;

/**
 * Created by Bi on 7/10/2017.
 */
public interface PhuongXaService {
    List<PhuongXaDTO> getAllItem();

    PhuongXaDTO getItemById(Long id);

    PhuongXaDTO addItem(PhuongXaDTO itemDTO);

    PhuongXaDTO updateItem(Long id, PhuongXaDTO itemDTO);

    boolean isItemExistByItem(PhuongXaDTO itemDTO);

    List<PhuongXaDTO> getItemByIdQuanHuyen(Long id);
}
