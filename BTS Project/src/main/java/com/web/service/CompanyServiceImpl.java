package com.web.service;

import com.web.common.CurrentDate;
import com.web.dto.CompanyDTO;
import com.web.entity.Company;
import com.web.repository.BTSRepository;
import com.web.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 7/10/2017.
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private BTSRepository btsRepository;

    private CompanyDTO transferEntitytoDTO(Company item) {
        CompanyDTO itemDTO = new CompanyDTO();
        itemDTO.setId(item.getId());
        itemDTO.setName(item.getName());
        itemDTO.setMarkericon(item.getMarkericon());
        itemDTO.setEmail(item.getEmail());
        itemDTO.setPhone(item.getPhone());
        itemDTO.setNote(item.getNote());
        itemDTO.setDateCreated(item.getDateCreated());
        itemDTO.setDateUpdated(item.getDateUpdated());
        // Số lượng trạm BTS
        itemDTO.setQuantityBTSOwner(btsRepository.countBTSByIdOwner(item.getId()));
        itemDTO.setQuantityBTSUser(btsRepository.countBTSByIdUser(item.getId()));
        return itemDTO;
    }


    private Company transferDTOtoEntity(CompanyDTO itemDTO) {
        Company item = new Company();
        item.setId(itemDTO.getId());
        item.setName(itemDTO.getName());
        item.setMarkericon(itemDTO.getMarkericon());
        item.setEmail(itemDTO.getEmail());
        item.setPhone(itemDTO.getPhone());
        item.setNote(itemDTO.getNote());
        item.setDateCreated(itemDTO.getDateCreated());
        item.setDateUpdated(itemDTO.getDateUpdated());
        return item;
    }

    @Override
    public List<CompanyDTO> getAllItem() {
        List<Company> list = new ArrayList<>();
        companyRepository.findAll().forEach(list::add);
        List<CompanyDTO> listDTO = new ArrayList<>();
        for (Company item : list) {
            CompanyDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public CompanyDTO getItemById(Long id) {
        Company item = companyRepository.findOne(id);
        CompanyDTO itemDTO = transferEntitytoDTO(item);
        return itemDTO;
    }

    @Override
    public CompanyDTO addItem(CompanyDTO itemDTO) {
        itemDTO.setDateCreated(CurrentDate.getCurrentSQLDate());
        Company item = transferDTOtoEntity(itemDTO);
        Company done = companyRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public CompanyDTO updateItem(Long id, CompanyDTO itemDTO) {
        itemDTO.setId(id);
        Company temp = companyRepository.findOne(id);
        itemDTO.setDateUpdated(CurrentDate.getCurrentSQLDate());
        Company item = transferDTOtoEntity(itemDTO);
        item.setDateCreated(temp.getDateCreated());
        Company done = companyRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public boolean isItemExistByItem(CompanyDTO itemDTO) {
        Company item = null;
        if (itemDTO.getId() == null) {
            item = companyRepository.isItemExistByNameOrMarker(itemDTO.getName(), itemDTO.getMarkericon());
        } else {
            item = companyRepository.isItemExistByIdNameOrMarker(itemDTO.getId(), itemDTO.getName(), itemDTO.getMarkericon());
        }
        if (item != null) return true;
        return false;
    }

}
