package com.web.service;

import com.web.dto.PhuongXaDTO;
import com.web.entity.PhuongXa;
import com.web.entity.QuanHuyen;
import com.web.repository.BTSRepository;
import com.web.repository.PhuongXaRepository;
import com.web.repository.QuanHuyenRepository;
import com.web.common.CurrentDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 7/10/2017.
 */
@Service
public class PhuongXaServiceImpl implements PhuongXaService {

    @Autowired
    private PhuongXaRepository phuongXaRepository;
    @Autowired
    private QuanHuyenRepository quanHuyenRepository;
    @Autowired
    private BTSRepository btsRepository;

    private PhuongXaDTO transferEntitytoDTO(PhuongXa item){
        PhuongXaDTO itemDTO = new PhuongXaDTO();
        itemDTO.setId(item.getId());
        itemDTO.setName(item.getName());
        itemDTO.setDateCreated(item.getDateCreated());
        itemDTO.setDateUpdated(item.getDateUpdated());
        itemDTO.setIdQuanHuyen(item.getIdQuanHuyen().getId());
        itemDTO.setNameQuanHuyen(item.getIdQuanHuyen().getName());
        // Số lượng trạm BTS
        itemDTO.setQuantityBTS(btsRepository.countBTSByIdPhuongXa(item.getId()));
        return itemDTO;
    }


    private PhuongXa transferDTOtoEntity(PhuongXaDTO itemDTO){
        PhuongXa item = new PhuongXa();
        item.setId(itemDTO.getId());
        item.setName(itemDTO.getName());
        item.setDateCreated(itemDTO.getDateCreated());
        item.setDateUpdated(itemDTO.getDateUpdated());
        QuanHuyen quanHuyen = quanHuyenRepository.findOne(itemDTO.getIdQuanHuyen());
        item.setIdQuanHuyen(quanHuyen);
        return item;
    }

    @Override
    public List<PhuongXaDTO> getAllItem() {
        List<PhuongXa> list = phuongXaRepository.getAllOrderByIdQuanHuyenName();
        List<PhuongXaDTO> listDTO = new ArrayList<>();
        for(PhuongXa item: list){
            PhuongXaDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public PhuongXaDTO getItemById(Long id) {
        PhuongXa item = phuongXaRepository.findOne(id);
        PhuongXaDTO itemDTO = transferEntitytoDTO(item);
        return itemDTO;
    }

    @Override
    public PhuongXaDTO addItem(PhuongXaDTO itemDTO) {
        itemDTO.setDateCreated(CurrentDate.getCurrentSQLDate());
        PhuongXa item = transferDTOtoEntity(itemDTO);
        PhuongXa done = phuongXaRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public PhuongXaDTO updateItem(Long id, PhuongXaDTO itemDTO) {
        itemDTO.setId(id);
        PhuongXa temp = phuongXaRepository.findOne(id);
        itemDTO.setDateUpdated(CurrentDate.getCurrentSQLDate());
        PhuongXa item = transferDTOtoEntity(itemDTO);
        item.setDateCreated(temp.getDateCreated());
        PhuongXa done = phuongXaRepository.save(item);
        itemDTO = transferEntitytoDTO(done);
        return itemDTO;
    }

    @Override
    public boolean isItemExistByItem(PhuongXaDTO itemDTO) {
        PhuongXa item = null;
        if(itemDTO.getId() != null){
            item = phuongXaRepository.isItemExistByIdQuanHuyenName(itemDTO.getIdQuanHuyen(), itemDTO.getName());
        }
//        else{
//            item = phuongXaRepository.isItemExistByName(itemDTO.getName());
//        }
        if(item != null) return true;
        return false;
    }
    @Override
    public List<PhuongXaDTO> getItemByIdQuanHuyen(Long id) {
        List<PhuongXa> list = phuongXaRepository.getListByIdQuanHuyen(id);
        List<PhuongXaDTO> listDTO = new ArrayList<>();
        for(PhuongXa item: list){
            PhuongXaDTO itemDTO = transferEntitytoDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }
}
