package com.web.dto;

import java.sql.Date;

/**
 * Created by Bi on 6/10/2017.
 */
public class PhuongXaDTO {
    /*Khi create hoặc update, các tham số cần truyền vào từ backend là (id) name, idQuanHuyen*/
    private Long id;

    private String name;

    private Date dateCreated;

    private Date dateUpdated;

    private Long idQuanHuyen;

    private String nameQuanHuyen;

    private int quantityBTS;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getIdQuanHuyen() {
        return idQuanHuyen;
    }

    public void setIdQuanHuyen(Long idQuanHuyen) {
        this.idQuanHuyen = idQuanHuyen;
    }

    public String getNameQuanHuyen() {
        return nameQuanHuyen;
    }

    public void setNameQuanHuyen(String nameQuanHuyen) {
        this.nameQuanHuyen = nameQuanHuyen;
    }

    public int getQuantityBTS() {
        return quantityBTS;
    }

    public void setQuantityBTS(int quantityBTS) {
        this.quantityBTS = quantityBTS;
    }
}
