package com.web.dto;

import java.sql.Date;

/**
 * Created by Bi on 6/10/2017.
 */
public class CompanyDTO {



    private Long id;
    private String name;
    private String markericon;
    private String email;
    private String phone;
    private String note;
    /*Không Truyền */

    private Date dateCreated;
    private Date dateUpdated;
    private int quantityBTSOwner;
    private int quantityBTSUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarkericon() {
        return markericon;
    }

    public void setMarkericon(String markericon) {
        this.markericon = markericon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getQuantityBTSOwner() {
        return quantityBTSOwner;
    }

    public void setQuantityBTSOwner(int quantityBTSOwner) {
        this.quantityBTSOwner = quantityBTSOwner;
    }

    public int getQuantityBTSUser() {
        return quantityBTSUser;
    }

    public void setQuantityBTSUser(int quantityBTSUser) {
        this.quantityBTSUser = quantityBTSUser;
    }
}
