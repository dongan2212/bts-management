package com.web.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

/**
 * Created by Bi on 6/10/2017.
 */
@Entity
@Table(name = "phuongxa")
public class PhuongXa {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "datecreated")
    private Date dateCreated;
    @Column(name = "dateupdated")
    private Date dateUpdated;
    @OneToMany(mappedBy = "idPhuongXa")
    private Set<BTS> BTSByIdPhuongXa;
    @ManyToOne
    @JoinColumn(name = "idquanhuyen")
    private QuanHuyen idQuanHuyen;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Set<BTS> getBTSByIdPhuongXa() {
        return BTSByIdPhuongXa;
    }

    public void setBTSByIdPhuongXa(Set<BTS> BTSByIdPhuongXa) {
        this.BTSByIdPhuongXa = BTSByIdPhuongXa;
    }

    public QuanHuyen getIdQuanHuyen() {
        return idQuanHuyen;
    }

    public void setIdQuanHuyen(QuanHuyen idQuanHuyen) {
        this.idQuanHuyen = idQuanHuyen;
    }
}
