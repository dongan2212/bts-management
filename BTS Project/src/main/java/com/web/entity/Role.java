//package com.web.entity;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.Set;
//
///**
// * Created by Bi on 5/10/2017.
// */
//@Entity
//@Table(name = "roles")
//public class Role implements Serializable {
//
//
//    private static final long serialVersionUID = 1L;
//
//    @Id
//    @GeneratedValue
//    private Long id;
//    String name;
//
//    Role() {}
//
//    public Role(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//}