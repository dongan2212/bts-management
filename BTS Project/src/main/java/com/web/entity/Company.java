package com.web.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

/**
 * Created by Bi on 6/10/2017.
 */
@Entity
@Table(name = "company")
public class Company implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "markericon")
    private String markericon;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;
    @Column(name = "note")
    private String note;
    @Column(name = "datecreated")
    private Date dateCreated;
    @Column(name = "dateupdated")
    private Date dateUpdated;
    @OneToMany(mappedBy = "idOwner")
    private Set<BTS> BTSByIdOwner;
    @OneToMany(mappedBy = "idUser")
    private Set<BTS> BTSByIdUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMarkericon() {
        return markericon;
    }

    public void setMarkericon(String markericon) {
        this.markericon = markericon;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Set<BTS> getBTSByIdOwner() {
        return BTSByIdOwner;
    }

    public void setBTSByIdOwner(Set<BTS> BTSByIdOwner) {
        this.BTSByIdOwner = BTSByIdOwner;
    }

    public Set<BTS> getBTSByIdUser() {
        return BTSByIdUser;
    }

    public void setBTSByIdUser(Set<BTS> BTSByIdUser) {
        this.BTSByIdUser = BTSByIdUser;
    }
}
