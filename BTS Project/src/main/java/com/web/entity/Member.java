package com.web.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Bi on 5/11/2017.
 */
@Entity
@Table(name = "member")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String name;
    private String email;

    @OneToMany(mappedBy = "userCreated", fetch = FetchType.LAZY)
    private Set<BTS> BTSbyUserCreated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<BTS> getBTSbyUserCreated() {
        return BTSbyUserCreated;
    }

    public void setBTSbyUserCreated(Set<BTS> BTSbyUserCreated) {
        this.BTSbyUserCreated = BTSbyUserCreated;
    }
}
