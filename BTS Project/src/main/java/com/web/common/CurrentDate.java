package com.web.common;

import java.util.Date;

/**
 * Created by Bi on 6/10/2017.
 */
public class CurrentDate {


    public static java.sql.Date getCurrentSQLDate(){
        java.util.Date uDate = new Date();
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }
}
