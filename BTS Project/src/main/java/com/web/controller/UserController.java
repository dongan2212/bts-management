package com.web.controller;

import com.web.common.AuthTokenInfo;
import com.web.common.ErrorException;
import com.web.common.ErrorResponse;
import com.web.dto.UserMemberDTO;
import com.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 3/11/2017.
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    private UserService userService;

//    @Autowired
//    private TokenStore tokenStore;


    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllItems() {
        return new ResponseEntity<>(userService.getAllItem(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getItemById(@PathVariable("id") Long id) throws ErrorException {
        if (userService.getItemById(id) == null) {
            throw new ErrorException("Tài khoản này không tồn tại");
        }
        return new ResponseEntity<>(userService.getItemById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/username/{username}")
    public ResponseEntity getItemByUsername(@PathVariable("username") String username) throws ErrorException {
        if (userService.getItemByUsername(username) == null) {
            throw new ErrorException("Tài khoản này không tồn tại");
        }
        return new ResponseEntity<>(userService.getItemByUsername(username), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewItem(@Validated @RequestBody UserMemberDTO item) throws ErrorException {
        if (userService.isItemExistByItem(item)) {
            throw new ErrorException("Tên đăng nhập bị trùng lặp tài khoản khác");
        }
        return new ResponseEntity<>(userService.addItem(item), HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/{id}/{username}")
    public ResponseEntity updateItem(@PathVariable("id") Long id,
                                     @PathVariable("username") String username,
                                     @Validated @RequestBody UserMemberDTO item) throws ErrorException {
        item.setId(id);
        if (userService.getItemById(id) == null) {
            throw new ErrorException("Tài khoản này không tồn tại");
        }
        if (!username.equals(item.getUsername())) {
            String t = item.getUsername();
            throw new ErrorException("Chỉ được phép cập nhật thông tin tài khoản cá nhân");
        }
        return new ResponseEntity<>(userService.updateItem(id, item, username), HttpStatus.OK);
    }

//    @RequestMapping(value = "/api/logout", method = RequestMethod.POST)
//    public void logout(@RequestBody AuthTokenInfo authTokenInfo) throws ErrorException {
//        OAuth2AccessToken auth2AccessToken = tokenStore.readAccessToken(authTokenInfo.getAccess_token());
//        OAuth2RefreshToken auth2RefreshToken = tokenStore.readRefreshToken(authTokenInfo.getRefresh_token());
//        if (auth2AccessToken == null || auth2RefreshToken == null) {
//            throw new ErrorException("Access token hoặc refresh token không đúng!");
//        } else {
//            tokenStore.removeAccessToken(auth2AccessToken);
//            tokenStore.removeRefreshToken(auth2RefreshToken);
//            throw new ErrorException("Đăng xuất thành công!");
//        }
//    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
