package com.web.controller;

import com.web.dto.CompanyDTO;
import com.web.service.CompanyService;
import com.web.common.ErrorException;
import com.web.common.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 7/10/2017.
 */
@RestController
@RequestMapping(value = "/api/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllItems() {
        return new ResponseEntity<>(companyService.getAllItem(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getItemById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(companyService.getItemById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewItem(@Validated @RequestBody CompanyDTO item) throws ErrorException {
        if (companyService.isItemExistByItem(item)) {
            throw new ErrorException("Thông tin bị trùng lặp Doanh nghiệp khác");
        }
        return new ResponseEntity<>(companyService.addItem(item), HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/{id}")
    public ResponseEntity updateItem (@PathVariable("id") Long id, @Validated @RequestBody CompanyDTO item) throws ErrorException {
        item.setId(id);
        if (companyService.getItemById(id) == null ) {
            throw new ErrorException("Doanh nghiệp này không tồn tại");
        }
        if (companyService.isItemExistByItem(item) ) {
            throw new ErrorException("Thông tin bị trùng lặp Doanh nghiệp khác");
        }
        return new ResponseEntity<>(companyService.updateItem(id, item), HttpStatus.OK);
    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
