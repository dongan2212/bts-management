package com.web.controller;

import com.web.dto.BtsDTO;
import com.web.service.BTSService;
import com.web.common.ErrorException;
import com.web.common.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 8/10/2017.
 */
@RestController
@RequestMapping(value = "/api/bts")
    public class BTSController {


    @Autowired
    private BTSService btsService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllItems() {
        return new ResponseEntity<>(btsService.getAllItem(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getItemById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(btsService.getItemById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/quanhuyen/{id}")
    public ResponseEntity getItemByIdQuanHuyen(@PathVariable("id") Long id) {
        return new ResponseEntity<>(btsService.getItemByIdQuanHuyen(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/phuongxa/{id}")
    public ResponseEntity getItemByIdPhuongXa(@PathVariable("id") Long id) {
        return new ResponseEntity<>(btsService.getItemByIdPhuongXa(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/companyuser/{id}")
    public ResponseEntity getItemByIdUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(btsService.getItemByIdUser(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/companyowner/{id}")
    public ResponseEntity getItemByIdOwner(@PathVariable("id") Long id) {
        return new ResponseEntity<>(btsService.getItemByIdUser(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewItem(@Validated @RequestBody BtsDTO item) throws ErrorException {
        if (btsService.isItemExistByItem(item)) {
            throw new ErrorException("Thông tin bị trùng lặp trạm bts khác");
        }
        return new ResponseEntity<>(btsService.addItem(item), HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/{id}")
    public ResponseEntity updateItem (@PathVariable("id") Long id, @Validated @RequestBody BtsDTO item) throws ErrorException {
        item.setId(id);
        if (btsService.getItemById(id) == null ) {
            throw new ErrorException("Trạm bts này không tồn tại");
        }
        if (btsService.isItemExistByItem(item) ) {
            throw new ErrorException("Thông tin bị trùng lặp trạm bts khác");
        }
        return new ResponseEntity<>(btsService.updateItem(id, item), HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete (@PathVariable("id") Long id) throws ErrorException {
        if (btsService.getItemById(id) == null ) {
            throw new ErrorException("Trạm bts này không tồn tại");
        }
        btsService.deleteItem(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
