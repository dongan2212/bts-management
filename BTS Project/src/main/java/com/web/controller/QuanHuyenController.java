package com.web.controller;
import com.web.dto.QuanHuyenDTO;
import com.web.service.QuanHuyenService;

import com.web.common.ErrorException;
import com.web.common.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 6/10/2017.
 */
@RestController
@RequestMapping(value = "/api/quanhuyen")
public class QuanHuyenController {

    @Autowired
    private QuanHuyenService quanHuyenService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllItems() {
        return new ResponseEntity<>(quanHuyenService.getAllItem(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getItemById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(quanHuyenService.getItemById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewItem(@Validated @RequestBody QuanHuyenDTO item) throws ErrorException {
        if (quanHuyenService.isItemExistByItem(item)) {
            throw new ErrorException("Thông tin bị trùng lặp quận huyện khác");
        }
        return new ResponseEntity<>(quanHuyenService.addItem(item), HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/{id}")
    public ResponseEntity updateItem (@PathVariable("id") Long id, @Validated @RequestBody QuanHuyenDTO item) throws ErrorException {
        item.setId(id);
        if (quanHuyenService.getItemById(id) == null ) {
            throw new ErrorException("Quận/Huyện này không tồn tại");
        }
        if (quanHuyenService.isItemExistByItem(item) ) {
            throw new ErrorException("Thông tin bị trùng lặp Quận/Huyện khác");
        }
        return new ResponseEntity<>(quanHuyenService.updateItem(id, item), HttpStatus.OK);
    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
