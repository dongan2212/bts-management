package com.web.controller;

import com.web.dto.PhuongXaDTO;
import com.web.service.PhuongXaService;
import com.web.common.ErrorException;
import com.web.common.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 7/10/2017.
 */
@RestController
@RequestMapping(value = "/api/phuongxa")
public class PhuongXaController {
    @Autowired
    private PhuongXaService phuongXaService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllItems() {
        return new ResponseEntity<>(phuongXaService.getAllItem(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/quanhuyen/{id}")
    public ResponseEntity getItemByIdQuanHuyen(@PathVariable("id") Long id) {
        return new ResponseEntity<>(phuongXaService.getItemByIdQuanHuyen(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getItemById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(phuongXaService.getItemById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewItem(@Validated @RequestBody PhuongXaDTO item) throws ErrorException {
        if (phuongXaService.isItemExistByItem(item)) {
            throw new ErrorException("Thông tin bị trùng lặp quận huyện khác");
        }
        return new ResponseEntity<>(phuongXaService.addItem(item), HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/{id}")
    public ResponseEntity updateItem (@PathVariable("id") Long id, @Validated @RequestBody PhuongXaDTO item) throws ErrorException {
        item.setId(id);
        if (phuongXaService.getItemById(id) == null ) {
            throw new ErrorException("Phường/Xã này không tồn tại");
        }
        if (phuongXaService.isItemExistByItem(item) ) {
            throw new ErrorException("Thông tin bị trùng lặp Phường/Xã khác");
        }
        return new ResponseEntity<>(phuongXaService.updateItem(id, item), HttpStatus.OK);
    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
